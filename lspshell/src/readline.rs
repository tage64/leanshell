use anyhow::{Context, Result};
use rustyline::{error::ReadlineError, ExternalPrinter};
use std::{fs, path::PathBuf};

pub struct Readline {
    editor: rustyline::DefaultEditor,
    history_file: Option<PathBuf>,
}

impl Readline {
    pub fn new() -> Result<Self> {
        let mut editor = rustyline::Editor::new()?;
        let history_file = if let Some(dirs) = directories::ProjectDirs::from("", "", "leanshell") {
            if let Some(state_dir) = dirs.state_dir() {
                fs::create_dir_all(state_dir)?;
                let history_file = state_dir.join("history.txt");
                fs::File::options()
                    .create(true)
                    .append(true)
                    .open(&history_file)?;
                editor.load_history(&history_file).with_context(|| {
                    format!(
                        "Failed to create history file at {}",
                        history_file.display()
                    )
                })?;
                Some(history_file)
            } else {
                None
            }
        } else {
            None
        };
        Ok(Self {
            editor,
            history_file,
        })
    }

    pub fn create_external_printer(&mut self) -> Result<impl FnMut(String) -> Result<()>> {
        let mut printer = self.editor.create_external_printer()?;
        Ok(move |mut msg: String| {
            // Not really shure why but it seems as though the message needs to end with a newline.
            if !msg.ends_with('\n') {
                msg += "\n";
            }
            printer.print(msg).map_err(|e| e.into())
        })
    }

    pub fn readline(&mut self, prompt: &str) -> Result<Option<String>> {
        loop {
            match self.editor.readline(prompt) {
                Err(ReadlineError::Eof | ReadlineError::Interrupted) => break Ok(None),
                Err(ReadlineError::WindowResized) => continue,
                Err(e) => break Err(anyhow::Error::new(e)),
                Ok(text) if text.is_empty() => continue,
                Ok(mut text) => {
                    if text.ends_with('\\') {
                        text.pop();
                        text += "\n";
                        let Some(continuation) = self.readline("::: ")? else {
                            return Ok(None);
                        };
                        text += &continuation;
                    }
                    self.editor.add_history_entry(&text)?;
                    break Ok(Some(text));
                }
            }
        }
    }
}

impl Drop for Readline {
    fn drop(&mut self) {
        if let Some(x) = &self.history_file {
            self.editor.append_history(x).ok();
        }
    }
}
