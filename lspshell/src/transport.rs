use crate::msg::{Message, Notification, Request, Response};
use anyhow::{anyhow, bail, Context, Error, Result};
use crossbeam_channel::Sender;
use lsp_types::{lsp_notification, lsp_request};
use std::{
    error::Error as StdError,
    ffi::OsStr,
    io::{self, Read},
    process,
    sync::{Arc, Mutex, RwLock},
    thread,
};

type ResponseWaiters = Arc<Mutex<Vec<(i32, Sender<Result<Response, Option<Error>>>)>>>;
type NotificationChannels = Arc<
    Mutex<
        Vec<(
            &'static str,
            Box<dyn FnMut(Result<Notification, Option<Error>>) -> Result<(), ()> + Send + 'static>,
        )>,
    >,
>;

pub struct LspTransport {
    /// Handle to the LSP process.
    lsp_proc: process::Child,
    lsp_stdin: Mutex<io::BufWriter<process::ChildStdin>>,
    /// Whether the LSP server has exited.
    lsp_exited: Arc<RwLock<bool>>,
    /// The ID for the next request. This will be incremented by 1 for each request.
    next_id: Mutex<i32>,
    response_waiters: ResponseWaiters,
    notification_channels: NotificationChannels,
}

struct ReaderThread {
    lsp_stdout: io::BufReader<process::ChildStdout>,
    lsp_stderr: io::BufReader<process::ChildStderr>,
    lsp_exited: Arc<RwLock<bool>>,
    response_waiters: ResponseWaiters,
    notification_channels: NotificationChannels,
}

impl LspTransport {
    pub fn new(
        prog: impl AsRef<OsStr>,
        args: impl IntoIterator<Item = impl AsRef<OsStr>>,
    ) -> Result<Self> {
        let mut lsp_proc = process::Command::new(prog)
            .args(args)
            .stdin(process::Stdio::piped())
            .stdout(process::Stdio::piped())
            .stderr(process::Stdio::piped())
            .spawn()
            .context("Failed to spawn LSP subprocess")?;
        let lsp_stdin = lsp_proc
            .stdin
            .take()
            .context("Failed to take stdin of LSP subprocess")?;
        let lsp_stdin = Mutex::new(io::BufWriter::new(lsp_stdin));
        let lsp_stdout = lsp_proc
            .stdout
            .take()
            .context("Failed to take stdout of LSP subprocess")?;
        let lsp_stdout = io::BufReader::new(lsp_stdout);
        let lsp_stderr = lsp_proc
            .stderr
            .take()
            .context("Failed to take stderr of LSP subprocess")?;
        let lsp_stderr = io::BufReader::new(lsp_stderr);
        let lsp_exited = Arc::new(RwLock::new(false));
        let response_waiters = Arc::new(Mutex::new(Vec::new()));
        let notification_channels = Arc::new(Mutex::new(Vec::new()));
        let reader_thread = ReaderThread {
            lsp_stdout,
            lsp_stderr,
            lsp_exited: lsp_exited.clone(),
            response_waiters: response_waiters.clone(),
            notification_channels: notification_channels.clone(),
        };
        reader_thread.spawn();
        Ok(Self {
            lsp_proc,
            lsp_stdin,
            lsp_exited,
            next_id: Mutex::new(0),
            response_waiters,
            notification_channels,
        })
    }
}

impl ReaderThread {
    fn spawn(mut self) {
        thread::spawn(move || loop {
            match self.recv_msg() {
                Ok(Message::Response(resp)) => {
                    let mut response_waiters = self.response_waiters.lock().unwrap();
                    for (i, (id, sender)) in response_waiters.iter().enumerate() {
                        if resp.id == (*id).into() {
                            sender.send(Ok(resp)).ok();
                            response_waiters.swap_remove(i);
                            break;
                        }
                    }
                }
                Ok(Message::Notification(notif)) => {
                    let mut notification_channels = self.notification_channels.lock().unwrap();
                    for (i, (method, sender)) in notification_channels.iter_mut().enumerate() {
                        if *method == notif.method {
                            if let Err(()) = sender(Ok(notif)) {
                                let _ = notification_channels.swap_remove(i);
                            }
                            break;
                        }
                    }
                }
                Ok(_) => (),
                Err(e) => {
                    let mut lsp_exited = self.lsp_exited.write().unwrap();
                    let e = if *lsp_exited {
                        None
                    } else {
                        *lsp_exited = true;
                        let e: Box<dyn StdError + Send + Sync + 'static> = e.into();
                        let e: Arc<dyn StdError + Send + Sync + 'static> = e.into();
                        Some(e)
                    };
                    drop(lsp_exited);

                    // Send the error to all channels.
                    let mut response_waiters = self.response_waiters.lock().unwrap();
                    for (_, sender) in response_waiters.drain(..) {
                        sender.send(Err(e.clone().map(|e| e.into()))).ok();
                    }
                    let mut notification_channels = self.notification_channels.lock().unwrap();
                    for (_, mut sender) in notification_channels.drain(..) {
                        sender(Err(e.clone().map(|e| e.into()))).ok();
                    }
                    break;
                }
            }
        });
    }

    /// Given an error, enhance its message with the stderr from the LSP server.
    fn with_stderr(&mut self, e: impl Into<Error>) -> Error {
        let e = e.into();
        let mut buf = Vec::new();
        if let Err(_) = self.lsp_stderr.read_to_end(&mut buf) {
            return e;
        }
        let stderr = String::from_utf8_lossy(&buf);
        e.context(format!("LSP server STDERR: {stderr}"))
    }

    fn recv_msg(&mut self) -> Result<Message> {
        if *self.lsp_exited.read().unwrap() {
            anyhow::bail!("The LSP process has exited");
        }
        let res = match Message::read(&mut self.lsp_stdout) {
            Ok(Some(Message::Notification(n))) if n.is_exit() => {
                Err(anyhow!("Received exit notification          "))
            }
            Ok(Some(msg)) => Ok(msg),
            Ok(None) => Err(anyhow!("The STDOUT from the LSP server reached EOF")),
            Err(e) => Err(anyhow!("Reading from the LSP server's STDOUT failed: {e}")),
        };
        res.map_err(|e| {
            *self.lsp_exited.write().unwrap() = true;
            self.with_stderr(e)
        })
    }
}

impl LspTransport {
    fn send_msg(&self, msg: Message) -> Result<()> {
        if *self.lsp_exited.read().unwrap() {
            anyhow::bail!("The LSP process has exited");
        }
        msg.write(&mut *self.lsp_stdin.lock().unwrap())
            .map_err(|e| {
                *self.lsp_exited.write().unwrap() = true;
                e.into()
            })
    }

    pub fn notification_subscribe<Notif: lsp_types::notification::Notification>(
        &self,
        mut f: impl FnMut(Result<Notif::Params, Option<Error>>) -> Result<(), ()> + Send + 'static,
    ) -> Result<()> {
        let mut notification_channels = self.notification_channels.lock().unwrap();
        if *self.lsp_exited.read().unwrap() {
            bail!("LSP server has exited.");
        }
        notification_channels.push((
            Notif::METHOD,
            Box::new(move |notif| match notif {
                Err(e) => f(Err(e)),
                Ok(Notification { params, .. }) => match serde_json::from_value(params) {
                    Ok(x) => f(Ok(x)),
                    Err(e) => f(Err(Some(Error::new(e).context(format!(
                        "Failed to parse params for {} notification",
                        Notif::METHOD
                    ))))),
                },
            }),
        ));
        Ok(())
    }

    /// Send a request to the LSP server and wait for the response.
    pub fn request<Req: lsp_types::request::Request, T: serde::de::DeserializeOwned>(
        &self,
        params: Req::Params,
    ) -> Result<T> {
        let mut next_id = self.next_id.lock().unwrap();
        let id = *next_id;
        *next_id += 1;
        drop(next_id);

        let (tx, rx) = crossbeam_channel::bounded(1);
        let mut response_waiters = self.response_waiters.lock().unwrap();
        if *self.lsp_exited.read().unwrap() {
            bail!("LSP server has exited.");
        }
        response_waiters.push((id, tx));
        drop(response_waiters);

        self.send_msg(Request::new(id.into(), Req::METHOD, params).into())?;

        let resp = rx
            .recv()
            .unwrap()
            .map_err(|e| e.unwrap_or(anyhow!("LSP server exited")))?;
        if let Some(e) = resp.error {
            bail!("{}: {e}", Req::METHOD);
        }
        let json = resp.result.unwrap_or(serde_json::Value::Null);
        let parsed = serde_json::from_value(json)
            .with_context(|| format!("Failed to parse response for {} request", Req::METHOD))?;
        Ok(parsed)
    }

    pub fn notify<Notif: lsp_types::notification::Notification>(
        &self,
        params: Notif::Params,
    ) -> Result<()> {
        self.send_msg(Notification::new(Notif::METHOD, params).into())
    }

    /// Send an initialize request to the LSP server. This shall be called prior to any other
    /// request.
    pub fn initialize(
        &mut self,
        capabilities: lsp_types::ClientCapabilities,
    ) -> Result<lsp_types::InitializeResult> {
        let res = self.request::<lsp_request!("initialize"), _>(lsp_types::InitializeParams {
            process_id: Some(process::id()),
            capabilities,
            ..Default::default()
        })?;
        self.notify::<lsp_notification!("initialized")>(lsp_types::InitializedParams {})?;
        Ok(res)
    }

    /// Send a shutdown request to the LSP server. No further requests shall be made after this
    /// call.
    pub fn shutdown(&self) -> Result<()> {
        self.request::<lsp_request!("shutdown"), serde_json::Value>(())
            .map(|_| ())
    }
}

impl Drop for LspTransport {
    fn drop(&mut self) {
        log::debug!("Sending exit notification to LSP server");
        if let Err(e) = self.notify::<lsp_notification!("exit")>(()) {
            log::debug!("Sending exit notification failed: {e}");
            return;
        }
        log::debug!("Waiting for LSP server process to exit.");
        match self.lsp_proc.wait() {
            Ok(x) => log::debug!("LSP server process exited with exit code: {}", x),
            Err(e) => log::debug!("LSP server process failed to terminate: {e}"),
        }
    }
}
