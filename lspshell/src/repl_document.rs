use crate::LspTransport;
use anyhow::{anyhow, Context, Result};
use crossbeam_channel::{Receiver, RecvTimeoutError, Sender};
use lsp_types::*;
use ordinal::Ordinal;
use regex::Regex;
use std::{
    cmp::Ordering,
    fmt,
    sync::{Arc, Mutex, MutexGuard},
    time::Duration,
};
use uuid::Uuid;

const MONAD_TREE_SHOW_TIMEOUT: Duration = Duration::from_millis(500);
const LEAN_PREAMBLE_LINES: &[&str] = &["import Leanshell", "open FS", "open Missiles"];

pub struct ReplDocument {
    lsp: Arc<LspTransport>,
    document: Arc<Mutex<Document>>,
    monad_tree_receiver: Receiver<MonadTreeMsg>,
    language_id: String,
}

struct Document {
    text_segments: Vec<TextSegment>,
    version: i32,
    uri: Url,
    monad_tree_sender: Sender<MonadTreeMsg>,
}

#[derive(Debug)]
struct MonadTreeMsg {
    monad_tree: MonadTree,
    document_version: i32,
}

#[derive(Debug, serde::Deserialize)]
pub struct MonadTree {
    pub name: String,
    pub monad: String,
    pub children: Vec<MonadTree>,
}

#[derive(Debug)]
struct TextSegment {
    text: String,
    range: Range,
    diagnostics: Vec<Diagnostic>,
}

impl ReplDocument {
    fn new(mut lsp: LspTransport, language_id: String) -> Result<Self> {
        let res = lsp.initialize(ClientCapabilities {
            ..Default::default()
        })?;

        match res.capabilities.text_document_sync {
            Some(TextDocumentSyncCapability::Kind(x))
            | Some(TextDocumentSyncCapability::Options(TextDocumentSyncOptions {
                change: Some(x),
                ..
            })) if x == TextDocumentSyncKind::INCREMENTAL => (),
            _ => anyhow::bail!(
                "Incremental text document synchronization is not supported by the LSP server"
            ),
        }

        let uri = Url::parse(Uuid::new_v4().urn().to_string().as_str()).unwrap();

        let (monad_tree_sender, monad_tree_receiver) = crossbeam_channel::unbounded();

        let document = Arc::new(Mutex::new(Document {
            text_segments: Vec::new(),
            uri,
            version: 0,
            monad_tree_sender,
        }));
        Ok(Self {
            lsp: Arc::new(lsp),
            document,
            monad_tree_receiver,
            language_id,
        })
    }

    fn initialize(
        &mut self,
        printer: impl FnMut(Result<String>) -> Result<(), ()> + Send + 'static,
    ) -> Result<()> {
        let document = self.document.lock().unwrap();
        self.lsp
            .notify::<lsp_notification!("textDocument/didOpen")>(DidOpenTextDocumentParams {
                text_document: TextDocumentItem {
                    uri: document.uri.clone(),
                    language_id: self.language_id.clone(),
                    version: document.version,
                    text: "".to_string(),
                },
            })?;
        drop(document);

        self.append_text(
            LEAN_PREAMBLE_LINES
                .iter()
                .fold(String::new(), |text, line| text + line + "\n"),
        )?;

        subscribe_diagnostics_printer(printer, self.lsp.clone(), self.document.clone())?;

        self.request_monad_tree()?;

        Ok(())
    }

    pub fn new_init(
        lsp: LspTransport,
        language_id: String,
        printer: impl FnMut(Result<String>) -> Result<(), ()> + Send + 'static,
    ) -> Result<Self> {
        let mut self_ = Self::new(lsp, language_id)?;
        self_.initialize(printer)?;
        Ok(self_)
    }

    fn send_text(
        &self,
        document: &mut MutexGuard<Document>,
        text: impl Into<String>,
        range: Range,
    ) -> Result<()> {
        document.version += 1;
        self.lsp
            .notify::<lsp_notification!("textDocument/didChange")>(DidChangeTextDocumentParams {
                text_document: VersionedTextDocumentIdentifier {
                    uri: document.uri.clone(),
                    version: document.version,
                },
                content_changes: vec![TextDocumentContentChangeEvent {
                    text: text.into(),
                    range: Some(range),
                    range_length: None,
                }],
            })?;
        Ok(())
    }

    pub fn append_text(&mut self, text: String) -> Result<()> {
        let text = self.preprocess_text(text);

        let mut document = self.document.lock().unwrap();
        let start = document
            .text_segments
            .last()
            .map(|x| x.range.end)
            .unwrap_or_default();
        let end = Position {
            line: start.line + u32::try_from(text.matches('\n').count()).unwrap(),
            character: text
                .rsplit_once('\n')
                .map(|(_, line)| line.len().try_into().unwrap())
                .unwrap_or_else(|| start.character + u32::try_from(text.len()).unwrap()),
        };
        self.send_text(&mut document, &text, Range { start, end: start })?;
        let range = Range { start, end };
        document.text_segments.push(TextSegment {
            text,
            range,
            diagnostics: Vec::new(),
        });
        Ok(())
    }

    fn preprocess_text(&self, text: String) -> String {
        let re = Regex::new(r"(?m)^@(\S+ )").unwrap();
        let new_text = re.replace_all(&text, "#run $1");
        if new_text == text {
            text
        } else {
            new_text.into_owned()
        }
    }

    pub fn document(&self) -> String {
        self.document
            .lock()
            .unwrap()
            .text_segments
            .iter()
            .map(|x| x.text.as_str())
            .collect()
    }

    pub fn next_monad_tree(&self) -> Result<MonadTree> {
        loop {
            match self
                .monad_tree_receiver
                .recv_timeout(MONAD_TREE_SHOW_TIMEOUT)
            {
                Ok(MonadTreeMsg {
                    document_version,
                    monad_tree,
                }) if document_version == self.document.lock().unwrap().version => {
                    return Ok(monad_tree);
                }
                Ok(_) => (),
                Err(RecvTimeoutError::Timeout) => {
                    // Rewrite the #showMTree line.
                    self.request_monad_tree()?;
                }
                Err(RecvTimeoutError::Disconnected) => {
                    anyhow::bail!("Monad tree channel disconnected unexpectedly.")
                }
            }
        }
    }

    fn request_monad_tree(&self) -> Result<()> {
        let document_end = self
            .document
            .lock()
            .unwrap()
            .text_segments
            .last()
            .context("Document preamble failed")?
            .range
            .end;
        self.send_text(
            &mut self.document.lock().unwrap(),
            "#showMTree\n",
            Range {
                start: document_end,
                end: Position {
                    character: 0,
                    line: document_end.line + 1,
                },
            },
        )
    }

    fn shutdown(&mut self) -> Result<()> {
        self.lsp
            .notify::<lsp_notification!("textDocument/didClose")>(DidCloseTextDocumentParams {
                text_document: TextDocumentIdentifier {
                    uri: self.document.lock().unwrap().uri.clone(),
                },
            })?;
        self.lsp.shutdown()?;
        Ok(())
    }
}

impl Drop for ReplDocument {
    fn drop(&mut self) {
        self.shutdown().ok();
    }
}

fn subscribe_diagnostics_printer(
    mut printer: impl FnMut(Result<String>) -> Result<(), ()> + Send + 'static,
    lsp: Arc<LspTransport>,
    document: Arc<Mutex<Document>>,
) -> Result<()> {
    let lsp_clone = lsp.clone();
    lsp_clone.notification_subscribe::<lsp_notification!("textDocument/publishDiagnostics")>(
        move |notif| match notif {
            Err(Some(e)) => printer(Err(e)),
            Err(None) => Err(()),
            Ok(params) => {
                let mut document = document.lock().unwrap();
                if params.uri != document.uri {
                    return printer(Err(anyhow!(
                        "Bad URI in publishDiagnostics notification. Expected: {}, got: {}",
                        document.uri,
                        params.uri
                    )));
                }
                if let Some(version) = params.version {
                    if version != document.version {
                        log::debug!(
                            "Received diagnostic with outdated document version {version}. \
                             The current version is {}",
                            document.version
                        );
                        return Ok(());
                    }
                }
                let version = document.version;
                for diagnostic in params.diagnostics {
                    handle_diagnostic(&mut printer, &*lsp, &mut *document, diagnostic)?;
                    if document.version != version {
                        break;
                    }
                }
                Ok(())
            }
        },
    )
}

fn handle_diagnostic(
    printer: &mut (impl FnMut(Result<String>) -> Result<(), ()> + Send + 'static),
    lsp: &LspTransport,
    document: &mut Document,
    diagnostic: Diagnostic,
) -> Result<(), ()> {
    log::debug!("Received diagnostic: {diagnostic:?}");
    if diagnostic.severity == Some(DiagnosticSeverity::ERROR) || diagnostic.severity == None {
        // Find which text segment this diagnostic belongs to.
        let idx = match document
            .text_segments
            .binary_search_by(|TextSegment { range, .. }| cmp_range(range, &diagnostic.range))
        {
            Ok(idx) => idx,
            Err(idx)
                if idx == document.text_segments.len()
                    && idx > 0
                    && document.text_segments.last().unwrap().range.end
                        == diagnostic.range.start =>
            {
                idx - 1
            }
            Err(idx) => {
                log::error!("Got diagnostic with range: {:?}", diagnostic.range);
                log::error!(
                    "Closest segments are: {:?} and {:?}",
                    if idx == 0 {
                        None
                    } else {
                        document.text_segments.get(idx - 1)
                    },
                    document.text_segments.get(idx)
                );
                return printer(Err(anyhow!("Bad range in diagnostic")));
            }
        };

        let msg = if idx + 1 == document.text_segments.len() {
            format!("Error: {}", diagnostic.message)
        } else {
            format!(
                "Error starting at the {} last command: {:?}",
                Ordinal(document.text_segments.len() - idx),
                diagnostic.message,
            )
        };
        if !document.text_segments.is_empty() {
            let last_removed = document.text_segments.drain(idx..).last().unwrap();
            let remove_start = document
                .text_segments
                .last()
                .map(|x| x.range.end)
                .unwrap_or_default();
            let remove_end = last_removed.range.end;
            document.version += 1;
            if let Err(e) = lsp.notify::<lsp_notification!("textDocument/didChange")>(
                DidChangeTextDocumentParams {
                    text_document: VersionedTextDocumentIdentifier {
                        uri: document.uri.clone(),
                        version: document.version,
                    },
                    content_changes: vec![TextDocumentContentChangeEvent {
                        text: "".to_string(),
                        range: Some(Range {
                            start: remove_start,
                            end: remove_end,
                        }),
                        range_length: None,
                    }],
                },
            ) {
                return printer(Err(e));
            }
        }
        printer(Ok(msg))
    } else {
        let Some(last_segment) = document.text_segments.last_mut() else {
            return printer(Ok(format!("{:?}", diagnostic)));
        };

        if diagnostic.range.start == last_segment.range.end {
            match serde_json::from_str(&diagnostic.message) {
                Ok(monad_tree) => {
                    document
                        .monad_tree_sender
                        .send(MonadTreeMsg {
                            monad_tree,
                            document_version: document.version,
                        })
                        .ok();
                }
                Err(_) => {
                    log::warn!("Couldn't parse diagnostic passed the end of the document as a monad tree: {diagnostic:?}");
                }
            }
            return Ok(());
        }
        if !range_overlap(&diagnostic.range, &last_segment.range) {
            return Ok(());
        }

        if last_segment.diagnostics.contains(&diagnostic) {
            return Ok(());
        }

        let severity_text = match diagnostic.severity {
            None => "".to_string(),
            Some(x) if x == DiagnosticSeverity::INFORMATION => "".to_string(),
            Some(x) => format!("{x:?}: "),
        };
        printer(Ok(format!("{severity_text}{}", diagnostic.message)))?;
        log::debug!("Printing: {:?}", diagnostic);
        last_segment.diagnostics.push(diagnostic);
        Ok(())
    }
}

/// Compare two ranges and return [Ordering::Equal] iff this contains the start of other.
fn cmp_range(this: &Range, other: &Range) -> Ordering {
    if other.start < this.start {
        Ordering::Greater
    } else if this.end <= other.start {
        Ordering::Less
    } else {
        Ordering::Equal
    }
}

/// Check if two ranges overlap.
fn range_overlap(this: &Range, other: &Range) -> bool {
    this.start < other.end && other.start < this.end || this.start == other.start
}

impl fmt::Display for MonadTree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.children.is_empty() {
            write!(f, "{}", self.name)?;
        } else {
            write!(f, "⟨")?;
            write!(f, "{}: {}", self.name, self.children[0])?;
            for child in &self.children[1..] {
                write!(f, ", {child}")?;
            }
            write!(f, "⟩")?;
        }
        Ok(())
    }
}
