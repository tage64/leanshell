use lspshell::{LspTransport, Readline, ReplDocument};

fn main() -> anyhow::Result<()> {
    //let log_file = std::fs::File::create("leanshell.log")?;
    env_logger::builder()
        .format_timestamp(None)
        //.filter_level(log::LevelFilter::Debug)
        //.target(env_logger::Target::Pipe(Box::new(log_file)))
        .target(env_logger::Target::Stdout)
        .init();

    let mut rl = Readline::new()?;
    let mut rl_printer = rl.create_external_printer()?;
    let printer = move |x| {
        let msg = match x {
            Ok(x) => x,
            Err(e) => format!("Error {e}"),
        };
        rl_printer(msg).map_err(|_| ())
    };

    println!("Initializing the Lean language server...");
    let mut lsp = ReplDocument::new_init(
        LspTransport::new("lake", &["serve"])?,
        "lean".to_owned(),
        printer,
    )?;

    let monad_tree = lsp.next_monad_tree()?;
    let mut prompt = format!("[{monad_tree}]→ ");
    loop {
        match rl.readline(&prompt)? {
            None => break,
            Some(x) if x == "document" => println!("{}", lsp.document()),
            Some(text) => {
                lsp.append_text(text + "\n")?;
                let monad_tree = lsp.next_monad_tree()?;
                prompt = format!("[{monad_tree}]→ ");
            }
        }
    }

    println!("Exiting Leanshell...");
    drop(lsp);
    println!("Goodbye, World!");
    Ok(())
}
