import «Leanshell»

def main : IO UInt32 := do
  let child ← IO.Process.spawn {
    cmd := "cargo",
    args := #["run", "--manifest-path", "lspshell/Cargo.toml"],
    stdin := .inherit,
    stdout := .inherit,
    stderr := .inherit,
  }
  let exitCode ← child.wait
  pure exitCode
