private inductive Box : Type 2 where
  | mk {Ty : Type 1} (val : Ty)
  deriving TypeName

inductive ULower (α : Type 1) : Type where
  | private mk' (data : Dynamic)

def ULower.mk (x : α) : ULower α :=
  mk' <| Dynamic.mk <| Box.mk x

instance [Inhabited α] : Inhabited (ULower α) where
  default := ULower.mk default

unsafe def ULower.get! : ULower α → α
  | mk' x => match x.get? Box with
    | some (Box.mk x) => unsafeCast x
    | none => lcUnreachable

unsafe def ULower.getImpl [Inhabited α] : ULower α → α :=
  ULower.get!

@[implemented_by ULower.getImpl]
opaque ULower.get [Inhabited α] : ULower α → α
