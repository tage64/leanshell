import Lean.Data.Json.Basic
import Lean.Data.Json.FromToJson
import Std.Data.HashMap
import Leanshell.Control.Runner
import Leanshell.Control.Instances
import Leanshell.DynMonad
import Leanshell.ULower

abbrev RootM := BaseIO

def ChildAddr := Nat
  deriving Inhabited, DecidableEq, Repr, Hashable
def NodeAddr := List Nat
  deriving Inhabited, DecidableEq, Repr, Hashable
def TreeAddr := ChildAddr × NodeAddr
  deriving Inhabited, DecidableEq, Repr, Hashable
def TreeAddr.toNodeAddr : TreeAddr → NodeAddr
  | (x, xs) => x :: xs
instance : Coe TreeAddr NodeAddr :=
  ⟨TreeAddr.toNodeAddr⟩

inductive MNode where
  | mk (nextChildAddr : ChildAddr)
       (innerMonad outterMonad : DynMonad)
       (σ ε : Type)
       (runner : Runner innerMonad.get outterMonad.get σ ε)
       (state : σ)
       (children : List (ChildAddr × MNode))

def Children := List (ChildAddr × MNode)
  deriving Inhabited

structure MTree where
  children : Children
  nextChildAddr : ChildAddr
  names : Std.HashMap String (NodeAddr × DynMonad)
  addrs : Std.HashMap NodeAddr (String × DynMonad)
  deriving TypeName

namespace MNode

def nextChildAddr : MNode → ChildAddr
  | .mk x _ _ _ _ _ _ _ => x

def innerMonad : MNode → DynMonad
  | .mk _ x _ _ _ _ _ _ => x

def outterMonad : MNode → DynMonad
  | .mk _ _ x _ _ _ _ _ => x

def σ : MNode → Type
  | .mk _ _ _ x _ _ _ _ => x

def ε : MNode → Type
  | .mk _ _ _ _ x _ _ _ => x

def runner : (x : MNode) → Runner x.innerMonad.get x.outterMonad.get x.σ x.ε
  | .mk _ _ _ _ _ x _ _ => x

def state : (x : MNode) → x.σ
  | .mk _ _ _ _ _ _ x _ => x

def children : MNode → Children
  | .mk _ _ _ _ _ _ _ x => x

def setChildren : MNode → Children → MNode
  | .mk nca im om σ ε runner state _, children => .mk nca im om σ ε runner state children

def setNextChildAddr : MNode → Nat → MNode
  | .mk _ im om σ ε runner state children, nca => .mk nca im om σ ε runner state children

def setState : (self : MNode) → self.σ → MNode
  | .mk nca im om σ ε runner _ children, state => .mk nca im om σ ε runner state children

def modifyChildrenM (self : MNode) [Monad m] (f : Children → m (Children))
      : m (MNode) :=
  self.setChildren <$> f self.children
end MNode

namespace Children

def get? : Children → ChildAddr → Option (MNode)
  | [], _ => failure
  | (addr, x) :: (xs : Children), addr' =>
    if addr = addr' then pure x
    else xs.get? addr'

def modifyM (ch : Children) (addr : ChildAddr)
      [Monad m] (f : MNode → m (α × MNode))
      : OptionT m (α × Children) :=
  match ch with
  | [] => failure
  | (addr', x) :: (xs : Children) => do
    if addr = addr' then
      let (val, newX) ← f x
      let newChildren : Children := (addr, newX) :: xs
      pure (val, newChildren)
    else
      let (val, newXs) ← xs.modifyM addr f
      let newChildren : Children := (addr', x) :: newXs
      pure (val, newChildren)

partial def toJson (ch : Children) (tr : MTree) (parentAddr : NodeAddr)
      : List Lean.Json :=
  match ch with
  | [] => []
  | (idx, x) :: (xs : Children) => 
    let addr := parentAddr.append [idx]
    let (name, monad) := tr.addrs.find! addr
    let children := x.children.toJson tr addr |> Lean.toJson
    let xs := xs.toJson tr parentAddr
    xs.append [Lean.Json.mkObj [("name", name), ("monad", monad.display), ("children", children)]]

partial def show_ : Children → String
  | [] => ""
  | (_, x) :: (xs : Children) => s!"{x.innerMonad.display} [{x.children.show_}], {xs.show_}"

end Children

namespace MNode
def get? (node : MNode) : NodeAddr → Option (MNode)
  | [] => node
  | x :: xs => do
    let child ← node.children.get? x
    child.get? xs

def modifyM (self : MNode) (addr : NodeAddr)
      [Monad m] (f : MNode → m (α × MNode))
      : OptionT m (α × MNode) := do
  match addr with
  | [] => f self
  | x :: xs => do
    let some (val, newChildren) ← OptionT.run <| self.children.modifyM x (·.modifyM xs f) | failure
    pure (val, self.setChildren newChildren)

def modify (self : MNode) (addr : NodeAddr) (f : MNode → MNode) : Option (MNode) := do
  let ((), newNode) ← self.modifyM (m := Id) addr fun x => (Unit.unit, f x)
  pure newNode

def add (self : MNode) (parentAddr : NodeAddr) (x : MNode)
      : Option (ChildAddr × MNode) :=
  self.modifyM (m := Id) parentAddr fun node =>
    let self₂ := self.setChildren <| (node.nextChildAddr, x) :: node.children
    (node.nextChildAddr, self₂.setNextChildAddr (Nat.succ node.nextChildAddr))

end MNode

namespace MTree

def singleton : MTree :=
  { children := []
  , nextChildAddr := (0 : Nat)
  , names := Std.HashMap.ofList [("root", ([], mk<RootM))]
  , addrs := Std.HashMap.ofList [([], ("root", mk<RootM))] }

instance : Inhabited MTree :=
  ⟨singleton⟩

def setChildren (tr : MTree) (children : Children) : MTree :=
  { tr with children }

structure Idx (tr : MTree) : Type where
  addr : NodeAddr
  hIn : tr.addrs[addr] ≠ none

def idxFromAddr? (tr : MTree) (addr : NodeAddr) : Option tr.Idx :=
  match h : tr.addrs[addr] with
  | none => none
  | some _ => by
    apply pure
    apply Idx.mk addr
    simp [h]

def idxFromName? (tr : MTree) (name : String) : Option tr.Idx := do
  let addr ← tr.names[name].map Prod.fst
  tr.idxFromAddr? addr

def Idx.get' {tr : MTree} (idx : tr.Idx) : String × DynMonad :=
  match h : tr.addrs[idx.addr] with
  | none => False.elim <| idx.hIn h
  | some x => x

abbrev Idx.getMIdx {tr : MTree} (idx : tr.Idx) : DynMonad :=
  idx.get'.2

abbrev Idx.get {tr : MTree} (idx : tr.Idx) : (Type → Type) :=
  idx.getMIdx.get

abbrev Idx.name {tr : MTree} (idx : tr.Idx) : String :=
  idx.get'.1

def get? (tr : MTree) : TreeAddr → Option (MNode)
  | (childAddr, nodeAddr) => do
    let child ← tr.children.get? childAddr
    child.get? nodeAddr

def modifyM (tr : MTree) (addr : TreeAddr)
      [Monad m] (f : MNode → m (α × MNode))
      : OptionT m (α × MTree) := do
  let some (val, newChildren) ← OptionT.run <| tr.children.modifyM addr.1 (·.modifyM addr.2 f) | failure
  pure (val, tr.setChildren newChildren)

def toJson (tr : MTree) : Lean.Json :=
  let (name, monad) := tr.addrs.find! []
  let children := tr.children.toJson tr [] |> Lean.toJson
  Lean.Json.mkObj [("name", name), ("monad", monad.display), ("children", children)]

instance : Lean.ToJson MTree := ⟨toJson⟩

def show_ (tr : MTree) : String :=
  s!"MTree| {(mk<RootM).display} [{tr.children.show_}]"
end MTree

abbrev EStateT (σ ε : Type) (m : Type → Type) := StateT σ (ExceptT ε m)

namespace EStateT

def Id := EStateT Unit Empty

def orLeft [Monad m] (σ₁ σ₂ ε₁ ε₂ : Type) (x : EStateT σ₁ ε₁ m α)
      : EStateT (σ₁ × σ₂) ((ε₁ × σ₂) ⊕ (σ₁ × ε₂)) m α := do
  match ← x.run (← get).1 |>.run with
  | .ok (res, s) => do
    modify ({· with fst := s})
    pure res
  | .error e => throw (.inl (e, (← get).2))

def orRight [Monad m] (σ₁ σ₂ ε₁ ε₂ : Type) (x : EStateT σ₂ ε₂ m α)
      : EStateT (σ₁ × σ₂) ((ε₁ × σ₂) ⊕ (σ₁ × ε₂)) m α := do
  match ← x.run (← get).2 |>.run with
  | .ok (res, s) => do
    modify ({· with snd := s})
    pure res
  | .error e => throw (.inr ((← get).1, e))

def andLeft [Monad m] (σ₁ σ₂ ε₁ ε₂ : Type) (x : EStateT σ₁ ε₁ m α)
      : EStateT (σ₁ × σ₂) (ε₁ ⊕ (σ₁ × ε₂)) m α := do
  match ← x.run (← get).1 |>.run with
  | .ok (res, s) => do
    modify ({· with fst := s})
    pure res
  | .error e => throw (.inl e)

def runInEState (runner : Runner m n σ₁ ε₁) [Monad n] (comp : EStateT σ₂ ε₂ m α)
      : EStateT (σ₁ × σ₂) (ε₁ ⊕ (σ₁ × ε₂)) n α := do
  let ranComp : StateT σ₁ (ExceptT ε₁ n) _ :=
    runner.run <| ExceptT.run <| StateT.run comp (← get).2
  match ← ranComp.run (← get).1 |>.run with
  | .ok (.ok (res, s₂), s₁) => do
    set (s₁, s₂)
    pure res
  | .ok (.error e₂, s₁) => throw <| .inr (s₁, e₂)
  | .error e => throw <| .inl e
  
end EStateT

mutual
def Children.superσAux : List (ChildAddr × MNode) → Type
  | [] => Unit
  | (_, x) :: xs => x.superσ × Children.superσAux xs
def MNode.superσ : MNode → Type
  | .mk _ _ _ σ _ _ _ children => σ × Children.superσAux children
end
abbrev Children.superσ : Children → Type := Children.superσAux

mutual
def Children.superΕAux : List (ChildAddr × MNode) → Type
  | [] => Empty
  | (_, x) :: xs => x.superΕ × Children.superσAux xs ⊕ x.superσ × Children.superΕAux xs

def MNode.superΕ : MNode → Type
  | .mk _ _ _ σ ε _ _ children => ε ⊕ σ × Children.superΕAux children
end
abbrev Children.superΕ : Children → Type := Children.superΕAux

theorem Children.superσ.eqOfCons : Children.superσ ((addr, x) :: xs) = (x.superσ × Children.superσ xs) := by
  conv => lhs; unfold Children.superσ Children.superσAux

def Children.superσ.unfold (state : Children.superσ ((addr, x) :: xs))
      : x.superσ × Children.superσ xs :=
  cast eqOfCons state

theorem MNode.superσ.eq : MNode.superσ x = (x.σ × x.children.superσ) := by
  unfold MNode.superσ Children.superσ MNode.σ MNode.children
  split
  simp

def MNode.superσ.unfold (state : MNode.superσ self)
      : self.σ × self.children.superσ :=
  cast MNode.superσ.eq state

theorem Children.superΕ.eqOfCons : Children.superΕ ((addr, x) :: xs)
                                 = (x.superΕ × Children.superσ xs ⊕ x.superσ × Children.superΕ xs) := by
  conv =>
    lhs
    unfold Children.superΕ Children.superΕAux

def Children.superΕ.unfold (err : Children.superΕ ((addr, x) :: xs))
      : x.superΕ × Children.superσ xs ⊕ x.superσ × Children.superΕ xs :=
  cast eqOfCons err

theorem MNode.superΕ.eq : MNode.superΕ x
                        = (x.ε ⊕ x.σ × x.children.superΕ) := by
  unfold MNode.superΕ MNode.σ MNode.ε MNode.children
  split
  simp

def MNode.superΕ.unfold (err : MNode.superΕ x)
      : x.ε ⊕ x.σ × x.children.superΕ :=
  cast MNode.superΕ.eq err

mutual
partial def Children.liftComp (ch : Children) (addr : TreeAddr) (mIdx : DynMonad)
      (comp : mIdx.get α)
      : Option ((mIdx' : DynMonad) × EStateT ch.superσ ch.superΕ mIdx'.get α) :=
  match ch, addr with
  | [], _ => failure
  | (childAddr, x) :: (xs : Children), (childAddr', nodeAddr) => do
    if childAddr = childAddr' then
      let some headComp := x.liftComp nodeAddr mIdx comp | failure
      let totalComp := EStateT.orLeft x.superσ xs.superσ x.superΕ xs.superΕ headComp
      by
        apply pure
        apply Sigma.mk
        case a.fst => exact x.outterMonad
        apply flip cast totalComp
        congr
        . exact Children.superσ.eqOfCons.symm
        . exact Children.superΕ.eqOfCons.symm
    else
      let ⟨mIdx', tailComp⟩ ← xs.liftComp addr mIdx comp
      let some ⟨h⟩ := mIdx'.maybeEq x.outterMonad | failure
      let totalComp := EStateT.orRight x.superσ xs.superσ x.superΕ xs.superΕ tailComp
      by
        apply pure
        apply Sigma.mk
        case a.fst => exact x.outterMonad
        apply flip cast totalComp
        congr
        . exact Children.superσ.eqOfCons.symm
        . exact Children.superΕ.eqOfCons.symm
partial def MNode.liftComp (self : MNode) (addr : NodeAddr) (mIdx : DynMonad)
      (comp : mIdx.get α)
      : Option (EStateT self.superσ self.superΕ self.outterMonad.get α) := do
  let x? : Option $ (mIdx : DynMonad) × EStateT self.children.superσ self.children.superΕ mIdx.get α :=
    match addr with
    | [] => pure ⟨mIdx, liftM comp⟩
    | x :: xs => do
      self.children.liftComp (x, xs) mIdx comp
  let some ⟨mIdx, comp⟩ := x? | failure
  let some ⟨h⟩ := mIdx.maybeEq self.innerMonad | failure
  let comp : EStateT self.children.superσ self.children.superΕ self.innerMonad.get α := by
    apply flip cast comp
    simp [h]
  let ranComp := EStateT.runInEState self.runner comp
  by
    apply pure
    apply flip cast ranComp
    congr
    . exact MNode.superσ.eq.symm
    . exact MNode.superΕ.eq.symm
end

def MTree.superσ (tr : MTree) : Type :=
  tr.children.superσ

def MTree.superΕ (tr : MTree) : Type :=
  tr.children.superΕ

def MTree.liftComp (tr : MTree) (idx : tr.Idx)
      (comp : idx.get α)
      : Option $ EStateT tr.superσ tr.superΕ RootM α := do
  match idx.addr with
  | [] => do
    let ⟨h⟩ ← (mk<RootM).maybeEq idx.getMIdx
    let comp : RootM α := by
      apply flip cast comp
      congr
      unfold Idx.get
      simp at h
      rw [h]
    some <| liftM comp
  | x :: xs => do
    let addr : TreeAddr := (x, xs)
    let some ⟨mIdx', liftedComp⟩ := tr.children.liftComp addr idx.getMIdx comp | failure
    let ⟨h⟩ ← mIdx'.maybeEq (mk<RootM)
    by
      apply pure
      apply flip cast liftedComp
      congr

mutual
def Children.getSuperStateAux : (ch : List (ChildAddr × MNode)) → Children.superσ ch
  | [] => Unit.unit
  | (_, x) :: xs =>
    by
      apply flip cast (x.getSuperState, Children.getSuperStateAux xs)
      exact Children.superσ.eqOfCons.symm
def MNode.getSuperState : (self : MNode) → self.superσ
  | .mk _ _ _ _ _ _ state children => by
      apply flip cast (state, Children.getSuperStateAux children)
      unfold MNode.superσ Children.superσ
      simp
end
def Children.getSuperState : (ch : Children) → ch.superσ :=
  Children.getSuperStateAux

def MTree.getSuperState (tr : MTree) : tr.superσ :=
  tr.children.getSuperState


mutual
def Children.setSuperStateAux : (ch : List (ChildAddr × MNode)) → Children.superσ ch → Children
  | [], Unit.unit => []
  | (addr, x) :: xs, state =>
    let (headState, tailState) := state.unfold
    (addr, x.setSuperState headState) :: Children.setSuperStateAux xs tailState
def MNode.setSuperState : (self : MNode) → self.superσ → MNode
  | .mk nca im om σ ε runner _ children, superState =>
    let (state, childrenState) := superState.unfold
    .mk nca im om σ ε runner state (Children.setSuperStateAux children childrenState)
end
def Children.setSuperState : (ch : Children) → ch.superσ → Children :=
  Children.setSuperStateAux

def MTree.setSuperState (tr : MTree) (state : tr.superσ) : MTree :=
  tr.setChildren <| tr.children.setSuperState state

mutual
def Children.updateByErrAux : (ch : List (ChildAddr × MNode)) → Children.superΕ ch → Children
  | [], (e : Empty) => e.elim
  | (addr, x) :: (xs : Children), err =>
    match err.unfold with
    | .inl (e, s) =>
      let newXs := xs.setSuperState s
      match x.updateByErr e with
      | some x => (addr, x) :: newXs
      | none => newXs
    | .inr (s, e) => (addr, x.setSuperState s) :: Children.updateByErrAux xs e
def MNode.updateByErr : (x : MNode) → x.superΕ → Option (MNode)
  | .mk nca im om σ ε runner _ children, err =>
    match err.unfold with
    | .inl _ => failure
    | .inr (state, e) =>
      let newChildren := Children.updateByErrAux children e
      pure <| .mk nca im om σ ε runner state newChildren
end
def Children.updateByErr : (ch : Children) → ch.superΕ → Children :=
  Children.updateByErrAux

def MTree.updateByErr (tr : MTree) (err : tr.superΕ) : MTree :=
  tr.setChildren <| tr.children.updateByErr err

mutual
def Children.showErrAux : (ch : List (ChildAddr × MNode)) → Children.superΕ ch → TreeAddr × String
  | [], (e : Empty) => e.elim
  | (childAddr, x) :: xs, err =>
    match err.unfold with
    | .inl (e, _) =>
      let (nodeAddr, msg) := x.showErr e
      ((childAddr, nodeAddr), msg)
    | .inr (_, e) => Children.showErrAux xs e
def MNode.showErr : (x : MNode) → x.superΕ → NodeAddr × String
  | .mk _ _ _ _ _ runner _ children, err =>
    match err.unfold with
    | .inl e => ([], runner.showError.toString e)
    | .inr (_, e) =>
      let ((childAddr, tailAddr), msg) := Children.showErrAux children e
      (childAddr :: tailAddr, msg)
end
def Children.showErr : (ch : Children) → ch.superΕ → TreeAddr × String :=
  Children.showErrAux

def MTree.showErr (tr : MTree) (err : tr.superΕ) : TreeAddr × String :=
  tr.children.showErr err

abbrev MTree.Lower : Type := ULower MTree

def MTree.lower (tr : MTree) : MTree.Lower :=
  ULower.mk tr

namespace MTree.Lower
def get : MTree.Lower → MTree :=
  ULower.get

instance : Coe MTree MTree.Lower :=
  ⟨MTree.lower⟩

instance : Coe MTree.Lower MTree :=
  ⟨MTree.Lower.get⟩
end MTree.Lower

def MTree.runRootComp (tr : MTree) (comp : EStateT tr.superσ tr.superΕ RootM α)
      : RootM (MTree.Lower × (Except String α)) := do
  let initState := tr.getSuperState
  match ← comp.run initState |>.run with
  | .ok (x, newState) =>
    pure (tr.setSuperState newState, .ok x)
  | .error e => do
    let (addr, msg) := tr.showErr e
    let some (nodeName, _) := tr.addrs[(addr : NodeAddr)]
      | panic! "Invalid index."
    let msg := s!"@{nodeName}: {msg}"
    let tr := tr.updateByErr e
    let tr := { tr with names := tr.names.erase nodeName, addrs := tr.addrs.erase addr }
    pure (tr.lower, .error msg)

def MTree.runComp (tr : MTree) (idx : tr.Idx)
      (comp : idx.get α)
      : OptionT RootM (MTree.Lower × Except String α) := do
  let some rootComp := tr.liftComp idx comp | failure
  tr.runRootComp rootComp

def MTree.add (tr : MTree) (parentIdx : tr.Idx) (name : String) (mIdx : DynMonad)
      (runner : Runner mIdx.get parentIdx.get σ ε)
      : OptionT RootM (MTree.Lower × Except String Unit) := do
  match ← tr.runComp parentIdx runner.init with
  | (tr, .error e) => pure (tr, .error e)
  | (tr, .ok (.error e)) =>
    pure (tr, .error s!"@initialization of {name}: {runner.showError.toString e}")
  | (tr, .ok (.ok state)) => do
    let tr : MTree := tr
    let newNode : MNode := .mk
      (innerMonad := mIdx) (outterMonad := parentIdx.getMIdx)
      (σ := σ) (ε := ε)
      (runner := runner) (state := state)
      (children := []) (nextChildAddr := (0 : Nat))
    let newTr : MTree :=
      match parentIdx.addr with
      | [] =>
        let addr := [tr.nextChildAddr]
        { children := (tr.nextChildAddr, newNode) :: tr.children
        , nextChildAddr := Nat.succ tr.nextChildAddr
        , names := tr.names.insert name (addr, mIdx)
        , addrs := tr.addrs.insert addr (name, mIdx) }
      | x :: xs =>
        match tr.children.modifyM x (·.add xs newNode) with
        | some none | none => panic! "MTree.add: Invalid index."
        | some (some (childAddr, children)) =>
          let addr : NodeAddr := parentIdx.addr.append [childAddr]
          { tr with 
            children
          , names := tr.names.insert name (addr, mIdx)
          , addrs := tr.addrs.insert addr (name, mIdx) }
    pure (newTr, .ok ())

mutual
def Children.removeAux : List (ChildAddr × MNode) → TreeAddr → Option (MNode × Children)
  | [], _ => failure
  | (addr, x) :: xs, (addr', addrTail) => do
    if addr = addr' then
      match addrTail with
      | [] => pure (x, xs)
      | y :: ys => do
        let (removed, updated) ← x.remove (y, ys)
        pure (removed, (addr, updated) :: xs)
    else
      let (removed, tail) ← Children.removeAux xs (addr', [])
      pure (removed, (addr, x) :: tail)
def MNode.remove : MNode → TreeAddr → Option (MNode × MNode)
  | .mk nca im om σ ε runner state children, addr => do
    let (removed, newChildren) ← Children.removeAux children addr
    pure (removed, .mk nca im om σ ε runner state newChildren)
end
def Children.remove : Children → TreeAddr → Option (MNode × Children) :=
  Children.removeAux

mutual
def Children.removeNames (tr : MTree) : NodeAddr → List (ChildAddr × MNode) → MTree
  | _, [] => tr
  | addr, (childAddr, x) :: xs =>
    let tr := MNode.removeNames tr (addr.append [childAddr]) x
    Children.removeNames tr addr xs
def MNode.removeNames (tr : MTree) : NodeAddr → MNode → MTree
  | addr, .mk _ _ _ _ _ _ _ children => Id.run do
    let some (name, _) := tr.addrs[addr]
      | panic! s!"MNode.removeNames: Invalid address: {reprStr addr}"
    let tr := { tr with names := tr.names.erase name, addrs := tr.addrs.erase addr }
    pure <| Children.removeNames tr addr children
end
    
def MTree.remove (tr : MTree) (idx : tr.Idx) : Except String MTree := do
  match idx.addr with
  | [] => throw "Cannot remove root."
  | x :: xs =>
    let addr : TreeAddr := (x, xs)
    let some (removedNode, children) := tr.children.remove addr
      | panic! s!"MTree.remove: Invalid address: {reprStr addr}"
    let tr : MTree := { tr with children }
    pure <| MNode.removeNames tr addr removedNode
