import Lean
import Lean.Elab
import Qq.ForLean.ToExpr
import Leanshell.ULower
open Lean Meta

structure DynMonad : Type 1 :=
  private mk'::
  expr : Expr
  normExpr : Expr  -- expr in normal form
  display : String
  get : Type → Type
  inst : Monad get

instance : Inhabited DynMonad where
  default := ⟨mkConst ``Id, mkConst ``Id, "Id", Id, inferInstance⟩

instance {m : DynMonad} : Monad m.get :=
  m.inst

unsafe def DynMonad.mk (expr : Expr) (m : Type → Type) [inst : Monad m] : MetaM (ULower DynMonad) := do
  let normExpr ← withDefault <| reduce expr
  let display ← ppExpr expr
  pure <| ULower.mk ⟨expr, normExpr, s!"{display}", m, inst⟩

unsafe def DynMonad.maybeEqImpl (x y : DynMonad) : Option $ PLift (x.get = y.get) :=
  if x.normExpr.eqv y.normExpr then
    some <| PLift.up lcProof
  else
    none

@[implemented_by DynMonad.maybeEqImpl]
opaque DynMonad.maybeEq (x y : DynMonad) : Option $ PLift (x.get = y.get)

def DynMonad.exprFromExpr (expr : Expr) : MetaM Expr := do
  let display ← ppExpr expr
  let instExpr ← mkAppOptM ``inferInstanceAs #[← mkAppM ``Monad #[expr], none]
  let normExpr ← withDefault <| reduce expr
  pure <| mkAppN (mkConst ``DynMonad.mk') #[toExpr expr, toExpr normExpr, toExpr s!"{display}", expr, instExpr]

/-def DynMonad.exprFromLocalName (name : Name) : MetaM Expr := do
  match ← resolveGlobalName name with
    | [] => throwError s!"{name}: No such declaration."
    | (fullName, fields) :: _ =>
      unless fields.isEmpty do
        throwError s!"{name} is not a name."
      DynMonad.exprFromName fullName
-/

def DynMonad.monadExpr (m : DynMonad) : Expr :=
  m.expr

elab "mk<" monad:term : term => do
  DynMonad.exprFromExpr (← Elab.Term.elabTerm monad none)

/-! ### Examples -/

namespace DynMonad.Test
def DynIO : DynMonad := mk<IO
example : DynIO.get = IO := rfl

opaque DynIO' : DynMonad := mk<IO
/-- We need a name without universe parameters, so we create a wrapper around `Option`. -/
abbrev OptionZero := Option.{0}
opaque DynOption : DynMonad := mk<OptionZero
example := true = (DynIO'.maybeEq DynIO |>.isSome)
example := false = (DynIO'.maybeEq DynOption |>.isSome)
end DynMonad.Test
