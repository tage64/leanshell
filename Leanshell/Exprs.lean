import Lean
import Leanshell.Control.Runner

open Lean Elab Meta Expr

def assertIsDefEq (x y : Expr) : MetaM Unit := do
  unless ← isDefEq x y do
    throwError s!"{x} and {y} are not definitionally equal."

/-- An expr of the form `Runner m n σ ε`. -/
structure RunnerTy where
  private mk::
  expr : Expr
  deriving Repr, Inhabited

namespace RunnerTy
instance : Coe RunnerTy Expr where
  coe x := x.expr

/-- Create an `RunnerTy` from an `Expr`, asserting that the form of the expr is valid. -/
def fromExpr (x : Expr) : MetaM RunnerTy := do
  unless x.isAppOfArity ``Runner 4 do
    throwError s!"Is not a valid RunnerTy: {x}"
  pure ⟨x⟩

def new (m n σ ε : Expr) : MetaM RunnerTy := do
  pure ⟨← mkAppM ``Runner #[m, n, σ, ε]⟩

/-- Get the inner monad, I.E. `m` in `Runner m n σ ε`. -/
def getInnerMonad (x : RunnerTy) : Expr :=
  x.expr.getAppArgs[0]!

/-- Get the outter monad, I.E. `n` in `Runner m n σ ε`. -/
def getOutterMonad (x : RunnerTy) : Expr :=
  x.expr.getAppArgs[1]!

/-- Get the state type, I.E. `σ` in `Runner m n σ ε`. -/
def getσ (x : RunnerTy) : Expr :=
  x.expr.getAppArgs[2]!

/-- Get the error type, I.E. `ε` in `Runner m n σ ε`. -/
def getε (x : RunnerTy) : Expr :=
  x.expr.getAppArgs[3]!

end RunnerTy

/-- An expression with type `Runner m n σ ε`. -/
structure RunnerExpr where
  private mk:: 
  expr : Expr
  type : RunnerTy
  deriving Inhabited, Repr

namespace RunnerExpr

/-- Create from an `Expr`, asserting the type is correct. -/
def fromExpr (expr : Expr) : MetaM RunnerExpr := do
  let type ← RunnerTy.fromExpr (← inferType expr)
  pure { expr, type }

end RunnerExpr
