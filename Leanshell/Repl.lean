import Lean
import Lean.Data.Json.Printer
import Leanshell.Control.Runner
import Leanshell.Control.Instances
import Leanshell.MTree
import Leanshell.Exprs
import Leanshell.ULower
import Leanshell.Show

open Lean Elab Command Meta Term

/-- Data which makes up the global state in the repl. -/
structure Data where
  /-- A tree of monads, runners and runner-states. -/
  tr : MTree.Lower
  deriving Inhabited
  
/-- Initialization of the global state, (I think it is initialized for each module). -/
initialize dataHandle : EnvExtension Data ←
  registerEnvExtension <| pure
    { tr := MTree.singleton }

/-- Possible errors in the Repl. -/
inductive Error where
  | InvalidIndex
  | TypeMismatch (expected : Expr) (got : Expr)
  | MonadMismatch (Expected : Expr) (got : Expr)
  | nodeNameAlreadyExists (name : String)
  | cannotRemoveRoot
  deriving Repr

/-- `ReplM` is a monad for computations that modify the global state in the repl. -/
abbrev ReplM : Type → Type := StateT Data $ ExceptT Error TermElabM

def ReplM.run (comp : ReplM α) : TermElabM α := do
  let data := dataHandle.getState (← getEnv)
  match ← StateT.run comp data |> ExceptT.run with
  | .ok (res, newData) => do
    modifyEnv (dataHandle.setState · newData)
    pure res
  | .error e => throwError s!"Error: {e |> reprStr}"

/-- Print the result of an IO computation. -/
unsafe def printIO (compExpr : Expr) : TermElabM Unit := do
  let type ← instantiateMVars (← inferType compExpr)
  unless type.isAppOfArity ``IO 1 do
    throwError s!"`printIO: Not an IO computation: {compExpr}"
  let mut stx ← Term.exprToSyntax compExpr
  unless (← isDefEq type.appArg! (mkConst ``Unit)) do
    stx ← `($stx >>= fun v => logInfo (reprStr v))
  let comp ← Term.evalTerm (IO Unit) (← mkAppM ``IO #[mkConst ``Unit]) stx
  comp

elab "#showMTree" : command => liftTermElabM <| ReplM.run do
  let data ← get
  let tr : MTree := data.tr
  logInfo <| tr.toJson |> toString

private def showComp [Show α] (m : _) [Monad m] (x : m α) : m String :=
  Functor.map showMe x

unsafe def runCompImpl (compExpr : Expr) (name : String) : ReplM Unit := do
  let data ← get
  let tr : MTree := data.tr
  let some idx := tr.idxFromName? name
    | Except.error Error.InvalidIndex
  let monadExpr := idx.getMIdx.monadExpr
  let type := mkAppN monadExpr #[mkConst ``String]
  
  -- Check that the computation is of the correct monad:
  let compTy ← inferType compExpr
  let α ← mkFreshExprMVar none
  unless ← isDefEq compTy (mkAppN monadExpr #[α]) do
    throwError "The computation {compExpr} : {compTy} must be in the {monadExpr} monad"
  let compExpr ← instantiateMVars compExpr
  let compStx ← exprToSyntax compExpr
  let monadStx ← exprToSyntax monadExpr
  let showedCompStx ← `(showComp $monadStx $compStx)
  let comp ← Term.evalTerm (idx.get String) type showedCompStx
  let ran := tr.runComp idx comp
  let ran := OptionT.run ran
  let some (newTr, res) ← ran
    | throwError "Internal error when running computation."
  match res with
    | .ok x => logInfo x
    | .error e => logInfo s!"Error: {e}"
  set ({ tr := newTr } : Data)
    
@[implemented_by runCompImpl]
opaque runComp (compExpr : Expr) (name : String) : ReplM Unit

elab "#run " monad:ident comp:term : command => liftTermElabM do
  let .str .anonymous monadName := monad.getId | throwError "Bad monad name."
  let compExpr ← Term.elabTerm comp none
  runComp compExpr monadName |> ReplM.run

unsafe def addMonadImpl (runnerTerm? : Option Syntax) (innerMonadExpr : Expr) (name : String) (parentName : String) : ReplM PUnit := do
  let innerMIdxExpr ← DynMonad.exprFromExpr innerMonadExpr 
  let dynMonadLowerTy ← mkAppM ``ULower #[mkConst ``DynMonad]
  let innerMIdxLowerExpr ← mkAppM ``ULower.mk #[innerMIdxExpr]
  let innerMIdxLower ← evalExpr (ULower DynMonad) dynMonadLowerTy innerMIdxLowerExpr
  let innerMIdx := innerMIdxLower.get!

  let data ← get
  let tr : MTree := data.tr
  let some parentIdx := tr.idxFromName? parentName
    | Except.error Error.InvalidIndex
  let parentMonadExpr := parentIdx.getMIdx.monadExpr
  let innerMonadExpr := innerMIdx.monadExpr
  let partialRunnerTy : RunnerTy ← do
    let mkTypeZeroMVar := do
      let mvar ← mkFreshExprMVar <| Expr.sort levelOne
      mkAppM ``outParam #[mvar]
    RunnerTy.new innerMonadExpr parentMonadExpr (← mkTypeZeroMVar) (← mkTypeZeroMVar)
  let runnerExpr : Expr ← match runnerTerm? with
    | some x => elabTerm x (expectedType? := partialRunnerTy)
    | none => synthInstance partialRunnerTy
  let runnerExpr ← RunnerExpr.fromExpr runnerExpr
  assertIsDefEq runnerExpr.type.getOutterMonad parentMonadExpr
  assertIsDefEq runnerExpr.type.getInnerMonad innerMonadExpr
  let lowerTy ← mkAppM ``ULower #[.sort levelOne]
  let σLowerExpr ← mkAppM ``ULower.mk #[runnerExpr.type.getσ]
  let σLower ← evalExpr (ULower Type) lowerTy σLowerExpr
  let σ := σLower.get!
  let εLowerExpr ← mkAppM ``ULower.mk #[runnerExpr.type.getε]
  let εLower ← evalExpr (ULower Type) lowerTy εLowerExpr
  let ε := εLower.get!
  let runnerLowerTy ← mkAppM ``ULower #[runnerExpr.type]
  let runnerLowerExpr ← mkAppM ``ULower.mk #[runnerExpr.expr]
  let runnerLower ← evalExpr (ULower $ Runner innerMIdx.get parentIdx.get σ ε) runnerLowerTy runnerLowerExpr
  let runner := runnerLower.get!
  let res := OptionT.run <| tr.add parentIdx name innerMIdx runner
  let some (newTr, res) ← res | throwError "Internal index error."
  set ({ tr := newTr } : Data)
  match res with
    | .ok () => logInfo s!"Successfully added node {name}"
    | .error e => throwError e

@[implemented_by addMonadImpl]
opaque addMonad (runnerTerm? : Option Syntax) (innerMonadExpr : Expr) (name : String) (parentName : String) : ReplM PUnit

elab "#add " monad:term runner?:(" with " term)? " as " name:ident " at " parentName:ident : command => liftTermElabM do
  let .str .anonymous name := name.getId | throwError s!"Bad name: {name}"
  let .str .anonymous parentName := parentName.getId
    | throwError s!"Bad parentName: {parentName}"
  let monadExpr ← Term.elabTerm monad none
  addMonad (runnerTerm? := runner?) (innerMonadExpr := monadExpr) (name := name) (parentName := parentName) |>.run
  
elab "#addWith " monad:term " with " runner:term " as " name:ident " at " parentName:ident : command => liftTermElabM do
  let .str .anonymous name := name.getId | throwError s!"Bad name: {name}"
  let .str .anonymous parentName := parentName.getId
    | throwError s!"Bad parentName: {parentName}"
  let monadExpr ← Term.elabTerm monad none
  addMonad (runnerTerm? := runner) (innerMonadExpr := monadExpr) (name := name) (parentName := parentName) |>.run
  
elab "#rm " monad:ident : command => liftTermElabM <| ReplM.run do
  let .str .anonymous monadName := monad.getId | throwError "Bad monad name."
  let data ← get
  let tr : MTree := data.tr
  let some idx := tr.idxFromName? monadName
    | Except.error Error.InvalidIndex
  match tr.remove idx with
    | .error e => throwError e
    | .ok newTr =>
      set ({ data with tr := newTr } : Data)

example := inferInstanceAs <| MonadState _ (StateM Nat)
elab "instTest" : term => fun _ _ => do
  let mvar ← mkFreshTypeMVar
  let instTy ← mkAppM ``MonadState #[mvar, ← mkAppM ``StateM #[mkConst ``Nat]]
  synthInstance instTy
