import Leanshell.Control.Runner

namespace Missiles
/-- A class for monads capable of launching missiles. -/
class MonadMissiles (m : Type → Type _) extends Monad m where
  /-- Launch all the missiles, effectively destroying the enemy.
   - Beaware that this might cause severe civilian casualties. -/
  launchMissiles : m (Except Unit Unit)
export MonadMissiles (launchMissiles)

end Missiles
