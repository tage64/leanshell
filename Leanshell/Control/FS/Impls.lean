import Leanshell.Control.FS
import Leanshell.Control.Instances
import Leanshell.Control.IO
open System (FilePath)
open System.FilePath

namespace FS
structure OsFS (α : Type) : Type where
  private inner : StateT FilePath IO α
instance : Monad OsFS where
  pure x := ⟨pure x⟩
  bind x f := ⟨bind x.inner (OsFS.inner ∘ f)⟩
instance : MonadExceptOf IO.Error OsFS where
  throw e := ⟨throw e⟩
  tryCatch x h := ⟨tryCatch x.inner (OsFS.inner ∘ h)⟩
private instance : MonadStateOf FilePath OsFS where
  set x := ⟨set x⟩
  get := ⟨get⟩
  modifyGet f := ⟨modifyGet f⟩
private instance : MonadLift (StateT FilePath IO) OsFS where
  monadLift x := ⟨x⟩
private instance : MonadLiftT BaseIO OsFS := inferInstance
private instance : MonadLiftT IO OsFS := inferInstance

structure OsFile (α : Type) : Type where
  inner : ReaderT IO.FS.Handle IO α
  deriving Inhabited
instance : Monad OsFile where
  pure x := ⟨pure x⟩
  bind x f := ⟨bind x.inner (OsFile.inner ∘ f)⟩
instance : MonadExceptOf IO.Error OsFile where
  throw e := ⟨throw e⟩
  tryCatch x h := ⟨tryCatch x.inner (OsFile.inner ∘ h)⟩
private instance : MonadReaderOf IO.FS.Handle OsFile where
  read := ⟨read⟩
private instance : MonadLift (ReaderT IO.FS.Handle IO) OsFile where
  monadLift x := ⟨x⟩

instance IOFileImpl : MonadFileWrite OsFile where
  readBytes n := do
    let fh ← read
    let read ← fh.read <| min n USize.size |>.toUSize
    if h : read.size ≤ n then
      pure ⟨read, h⟩
    else panic! "Size read bytes is grater than the expected number of bytes."

  getLine := do
    let fh ← read
    try
      pure <| match ← fh.getLine with
        | "" => none
        | line => some line
    catch
      | IO.Error.unexpectedEof => pure none
      | e => throw e

  writeBytes bytes := do
    let fh ← read
    fh.write bytes

  putStr str := do
    let fh ← read
    fh.putStr str

  flush := do
    let fh ← read
    fh.flush

private def appendPath (path : FilePath) : OsFS FilePath := do
  let path := path.normalize
  let newPath ← if path.isAbsolute
    then pure path
    else do
      let mut newPath ← get
      for component in path.components do
        match component with
        | "." => pure ()
        | ".." => do
          match (newPath.parent, newPath.fileName) with
          | (_, some "..") | (none, _) =>
            newPath := newPath.join ".."
          | (some parent, _) =>
            newPath := parent
        | x => do
          newPath := newPath.join x
      pure newPath
  unless ← newPath.pathExists do
    throw (m := IO) s!"Path does not exist: {newPath}"
  pure newPath

instance IOFSImpl : MonadWriteFS OsFS where
  ls dir := do
    let paths ← (← appendPath dir).readDir
    pure <| Array.map (·.fileName) paths
  metadata x := do
    (← appendPath x).metadata
  pwd := get
  pathExists path := do
    (← appendPath path).pathExists
  cd dir := do
    let newPath ← appendPath dir
    set newPath
  rm path := do
    IO.FS.removeFile (← appendPath path)
  createDir path := do
    IO.FS.createDir <| (← appendPath path)

  mfr := OsFile
  mfw := OsFile
  σ_r := IO.FS.Handle
  σ_w := IO.FS.Handle
  ε_r := IO.Error
  ε_w := IO.Error

  readFile fName := {
    init := do
      let path ← appendPath fName
      IO.FS.Handle.mk path .read
    run := fun x => do
      let fh ← get
      liftM <| ReaderT.run x.inner fh
    finalize := default
  }

  writeFile fName := {
    init := do
      let path ← appendPath fName
      IO.FS.Handle.mk path .write
    run := fun x => do
      let fh ← get
      liftM <| ReaderT.run x.inner fh
    finalize := default
  }

example : Runner OsFS OsFS Unit IO.Error := inferInstance
instance osFSRunner : Runner OsFS BaseIO FilePath IO.Error where
  init := IO.currentDir.toBaseIO
  run comp := do
    let fh₁ ← get
    try
      let ⟨res, fh₂⟩ ← StateT.run (m := IO) comp.inner fh₁ |>.toBaseIO |> liftM (m := ExceptT IO.Error BaseIO)
      set fh₂
      pure res
    catch e =>
      throw (m := ExceptT IO.Error BaseIO) e
  finalize _ := pure ()

structure SubFS (m : Type → Type) (α : Type) : Type where
  private inner : ReaderT FilePath (ExceptT String m) α
instance [Monad m] : Monad (SubFS m) where
  pure x := ⟨pure x⟩
  bind x f := ⟨bind x.inner (SubFS.inner ∘ f)⟩
private instance [Monad m] : MonadReaderOf FilePath (SubFS m) where
  read := ⟨read⟩
private instance : MonadLift (ReaderT FilePath (ExceptT String m)) (SubFS m) where
  monadLift x := ⟨x⟩
private instance [Monad m] : MonadExceptOf String (SubFS m) where
  throw e := ⟨throw e⟩
  tryCatch x h := ⟨tryCatch x.inner (h · |>.inner)⟩

/-- Split an absolute path p, in the "real" file system, into components
-  and separate components for the base directory and the tail. -/
private def SubFS.baseAndRelativeComponents [Monad m] (p : FilePath)
      : (SubFS m) (List String × List String):= do
  let base ← read
  let baseComps := base.components
  let ⟨preComps, postComps⟩ := p.components.splitAt baseComps.length
  unless preComps = baseComps do
    throw s!"Path: {p}, is outside base directory: {base}"
  pure ⟨preComps, postComps⟩

private def SubFS.liftPath [MonadReadFS m] (p : FilePath) : SubFS m FilePath := do
  let pwd ← pwd
  let ⟨preComps, postComps⟩ ← baseAndRelativeComponents pwd
  let mut components := preComps.toArray
  let postComps := if p.isRelative
    then postComps ++ p.components
    else p.components
  for component in postComps do
    match component with
    | "" | "." => continue
    | ".." =>
      if components.size = preComps.length then
        throw s!"You are trying to go outside the base directory: {System.mkFilePath postComps}"
      else
        components := components.pop
    | c =>
      components := components.push c
  pure <| System.mkFilePath components.toList

private def SubFS.relativize [MonadReadFS m] (p : FilePath) : SubFS m FilePath := do
  if p.isRelative
  then pure p
  else
    let ⟨_, components⟩ ← baseAndRelativeComponents p
    -- Add a leading empty component to denote an absolute path:
    let components := if components == []
      then ["", ""]
      else ("" :: components)
    pure <| System.mkFilePath components

instance [MonadReadFS m] : MonadReadFS (SubFS m) where
  ls dir := do
    let xs ← ls (m := m) (← SubFS.liftPath dir)
    xs.mapM SubFS.relativize
  metadata x := do
    metadata (m := m) (← SubFS.liftPath x)
  pwd := do
    SubFS.relativize (← pwd (m := m))
  pathExists path := do
    pathExists (m := m) (← SubFS.liftPath path)
  cd dir := do
    cd (m := m) (← SubFS.liftPath dir)
  mfr := mfr (m := m)
  σ_r := MonadReadFS.σ_r (m := m)
  ε_r := MonadReadFS.ε_r (m := m)

  readFile fName := {
    init := do
      readFile (m := m) (← SubFS.liftPath (m := m) fName) |>.init
    run := fun x => do
      readFile (m := m) (← SubFS.liftPath (m := m) fName) |>.run x
    finalize := fun x => do
      readFile (m := m) (← SubFS.liftPath fName) |>.finalize x
    -- This is a really bad solution but it may be good enough for now:
    showError := readFile (m := m) "." |>.showError
  }

structure SubFS.σ where
  private inner : FilePath

def subFS [MonadReadFS m] (root : FilePath) : Runner (SubFS m) m SubFS.σ String := {
  init := do
    let absRoot := (← pwd).join root
    cd absRoot
    pure ⟨absRoot⟩
  run := fun comp => do
    liftM <| ReaderT.run comp.inner (← get).inner
  finalize := default
}
end FS
