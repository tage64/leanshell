import Leanshell.Control.FS
open FS

def File.print [MonadFileRead m] (max_lines : Option Nat := none) : m String := do
  let mut text := ""
  let mut remaining_lines := max_lines
  while remaining_lines ≠ some 0 do
    if let some line ← getLine then
      text := text.append line
      remaining_lines := remaining_lines.map (·.pred)
    else break
  pure text

def cat [MonadReadFS m] (fileName : FilePath) (max_lines : Option Nat := none)
      : ExceptT (MonadReadFS.ε_r m) m String := do
  readFile fileName |>.runWith' fun f => do
    f <| File.print (max_lines := max_lines)
  
