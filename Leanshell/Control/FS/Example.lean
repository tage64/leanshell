import Leanshell.Control.FS

open FS

example [fs : MonadReadFS m] : ExceptT (MonadReadFS.ε_r m) m (Option Nat) := do
  let files ← ls
  if files.contains "foo.txt" then
    readFile "foo.txt" |>.runWith fun fh => do
      match ← fh getLine with
        | some x => pure x.length
        | none =>
          pure (← fs.metadata "foo.txt").byteSize.toNat
  else
    return none
  
