def IO.toBaseIO (x : IO α) : ExceptT IO.Error BaseIO α :=
  EIO.toBaseIO x
