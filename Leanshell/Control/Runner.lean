import Std
import Leanshell.Control.Instances

/--
 - A class for running computations in a monad `m` by a monad `n` with initialization,
 - finalization and exceptions.
 -
 - This class contains two type members:
 - * `σ : Type _` is a state that will be produced by the `init` function and be used
 -   whenever the user wants to run some computation in `m`.
 - * `ε : Type _` is an error type used by the `init` and `run` functions.
 - 
 - In addition, there are three functions:
 - * `init : ExceptT ε n σ` producing the initial state.
 - * `run : m α → StateT σ (ExceptT ε n) α` running a computation in `m`.
 - * `finalize : σ → n PUnit` finalizing the run.
 -
 - A typical usage could look like this:
 - ```
 - example [Monad n] (readInt : m Int) (runM : Runner m n) : ExceptT runM.ε n Int := do
 -   let s₁ ← runM.init
 -   let (x, s₂) ← StateT.run (runM.run readInt) s₁
 -   let (y, s₃) ← StateT.run (runM.run readInt) s₂
 -   runM.finalize s₃
 -   pure (x + y)
 - ```
 -   
 - It is **important** that the state (`σ`) is used **linearly**. That is, every `s : σ`
 - must be used **exactly** once, neither more nor less. So for every invocation of `init`,
 - there should usually be one corresponding invocation of `finalize`.
 - Note however that if the `run` function failes, then no new state is returned so `finalize`
 - **must not** be called in that case.
 -
 - One could think of the state `σ` as the state of a program running in user mode.
 - Assuming there are no forks, a program cannot just be copied and should not be destroyed
 - unless it has cleaned up all its resources. The `run` function then runs a user level
 - program in the kernel. The error `ε` is a signal which makes the program impossible to continue.
 -/
class Runner (m : Type u → Type v) (n : Type u → Type w) 
             (σ ε : outParam (Type u)) where
  /-- The errors should be displayable. -/
  [showError : ToString ε]

  /-- Initialize the runner, returning the initial state. -/
  init : ExceptT ε n σ

  /-- Run a computation in `m` by `n`.
   - If an error is thrown (in the `ExceptT`) the state `σ` should NOT be used anymore,
   - it should not even be finalized. -/
  run : m α → StateT σ (ExceptT ε n) α

  /-- Finalize the runner, cleaning up any resources that might be hold by the state `σ`. -/
  finalize : σ → n PUnit

namespace Runner

/-- Run a computation in `m` by `n`, performing necessary initialization and finalization. -/
def completeRun [Monad n] [inst : Runner m n σ ε] (x : m α) : ExceptT ε n α := do
  let s ← inst.init
  let (res, sNew) ← StateT.run (inst.run x) s
  inst.finalize sNew
  pure res

def completeRun' [Monad n] [Runner m n σ ε] [MonadExcept ε n] (x : m α) : n α := do
  match ← completeRun x |> ExceptT.run  with
  | .ok x => pure x
  | .error e => throw e

/-- If `n` runs `m` and `n` can be lifted to `o`, then `o` runs `m` -/
instance lift [Monad n] [Monad o] [inst : Runner m n σ ε] [MonadLift n o] : Runner m o σ ε where
  showError := inst.showError
  init := inst.init |> liftM
  run x := inst.run x |> liftM
  finalize s := inst.finalize s |> liftM

instance id [Monad m] [ToString ε] [MonadExcept ε m] : Runner m m PUnit ε where
  init := pure default
  run x := do
    try
      let res ← x
      pure res
    catch e => throw (m := ExceptT ε m) e
  finalize _ := pure default

def runWith [Monad n] (runner : Runner m n σ ε)
            (f : ∀ {o}, [Monad o] → [MonadLiftT n o] → (∀ {α}, m α → o α) → o α)
            : ExceptT ε n α := do
  let o := StateT σ (ExceptT ε n)
  let state ← runner.init
  let comp : o α := f (o := o) fun x => runner.run x
  let (res, newState) ← comp.run state
  runner.finalize newState
  pure res
  
def runWith' [Monad n] (runner : Runner m n σ ε) [MonadExcept ε n]
            (f : ∀ {o}, [Monad o] → [MonadLiftT n o] → (∀ {α}, m α → o α) → o α)
            : n α := do
  match ← runner.runWith f |> ExceptT.run  with
  | .ok x => pure x
  | .error e => throw e

def runIn [Monad n] (m : Type → Type) [runner : Runner m n σ ε]
            (f : ∀ {o}, [Monad o] → [MonadLiftT n o] → (∀ {α}, m α → o α) → o α)
            : ExceptT ε n α :=
  runner.runWith f

def runIn' [Monad n] (m : Type → Type) [runner : Runner m n σ ε] [MonadExcept ε n]
            (f : ∀ {o}, [Monad o] → [MonadLiftT n o] → (∀ {α}, m α → o α) → o α)
            : n α :=
  runner.runWith' f
end Runner
