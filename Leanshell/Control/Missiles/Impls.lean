import Leanshell.Control.Missiles
import Leanshell.Control.IO

namespace Missiles
structure MissileSimulator (α : Type) : Type where
  private inner : StateT StdGen IO α

instance : Monad MissileSimulator where
  pure x := ⟨pure x⟩
  bind x f := ⟨bind x.inner (f ·|>.inner)⟩
private instance : MonadLift (StateT StdGen IO) MissileSimulator where
  monadLift x := ⟨x⟩

instance : MonadMissiles MissileSimulator where
  launchMissiles := do
    let gen ← get
    let ⟨fortune, newGen⟩ := randNat gen 1 6
    set newGen
    if fortune ≤ 2 then
      IO.println "Missile error: Failed to launch missiles"
      pure (.error ())
    else
      IO.println "Missiles launched successfully. This was a really terrible thing!"
      pure (.ok ())

instance runner : Runner MissileSimulator BaseIO StdGen IO.Error where
  init := pure mkStdGen
  run x := fun s => x.inner s |>.toBaseIO
  finalize := default
end Missiles
