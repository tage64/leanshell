/-! Some useful instances which the compiler must be reminded about. -/
instance [Monad m] [MonadLiftT m n] : MonadLiftT (ExceptT ε m) (ExceptT ε n) where
  monadLift x := x.run |> liftM |> ExceptT.mk

instance [Monad n] [MonadLiftT m n] : MonadLiftT (StateT σ m) (StateT σ n) where
  monadLift comp := do
    let state ← get
    let (res, newState) ← comp.run state
    set newState
    pure res

def liftInEState [Monad m] [Monad n] [MonadLiftT m n] {α} (x : StateT σ (ExceptT ε m) α) : StateT σ (ExceptT ε n) α :=
  liftM x
example [Monad m] (x : m α) : StateT σ₂ (ExceptT ε₂ (StateT σ (ExceptT ε m))) α :=
  liftM x

