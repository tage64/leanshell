import Leanshell.Control.Runner

/-!
This file implements an examplifying monad class for allocating integers on the heap and a monad
class for a counter. It then creates a `HeapCounter` which is a counter which stores its value on the heap. The soely purpose of this module
is to showcase how monad classes and runners are created and instantiated.
-/

namespace HeapCounter

private opaque PtrPointed : NonemptyType
/-- the type of a pointer -/
def Ptr : Type := PtrPointed.type

/-- a monad class for allocating integers on the heap -/
class MonadIntAlloc (m : Type → Type) extends Monad m where
  allocInt : m Ptr  -- return a pointer to an allocated integer on the heap
  readInt : Ptr → m Int  -- read the value stored at the pointer
  writeInt : Ptr → Int → m Unit  -- write a value to the integer stored at the pointer
  free : Ptr → m Unit  -- free the memory at the pointer
export MonadIntAlloc (allocInt readInt writeInt free)

/-- a monad class for a counter -/
class MonadCounter (m : Type → Type) extends Monad m where
  getCounter : m Int  -- get the current value in the counter
  incr : m Unit  -- increment the counter by one
export MonadCounter (getCounter incr)

instance : ToString Empty where
  toString e := e.rec

/-- a counter which stores its value on the heap -/
structure HeapCounter (m : Type → Type) (α : Type) where
  private inner : ReaderT Ptr m α
instance [Monad m] : Monad (HeapCounter m) where
  pure x := ⟨pure x⟩
  bind x f := ⟨bind x.inner (HeapCounter.inner ∘ f)⟩

instance [MonadIntAlloc m] : MonadCounter (HeapCounter m) where
  getCounter := do
    let ptr ← ⟨read⟩  -- Store the pointer in ptr.
    ⟨liftM (m := m) (readInt ptr)⟩  -- Return the int stored at ptr.
  incr := do
    let ptr ← ⟨read⟩  -- Store the pointer in ptr.
    let val ← ⟨liftM (m := m) (readInt ptr)⟩  -- Store the current counter value in val.
    -- Increase the counter value by 1:
    ⟨liftM (m := m) (writeInt ptr (val + 1))⟩

instance [MonadIntAlloc m] : Runner (HeapCounter m) m Ptr Empty where
  -- Initialization: Allocate a pointer on the heap.
  init := liftM (m := m) allocInt
  -- Run a computation of type `HeapCounter m` inside `StateT Ptr m`.
  run comp := do
    let ptr ← get  -- Store the pointer in ptr.
    -- Run the computation using ptr as pointer:
    liftM <| ReaderT.run comp.inner ptr
  -- Finalization: Free the pointer.
  finalize ptr :=
    liftM (m := m) <| free ptr
