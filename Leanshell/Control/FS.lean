import Init.System.IO
import Init.System.FilePath
import Std.Data.List.Basic
import Leanshell.Control.Runner
open System
export System (FilePath)

namespace FS


@[nolint docBlame]
def defaultBlockSize : Nat := 1024

/-- A class for monads with capabilities to read a file. -/
class MonadFileRead (m : Type → Type _) extends Monad m where
  /-- 
   - Try to read `n` bytes from a file and advance the read cursor in the file.
   - If zero bytes are read, EOF is reached.
   -/
  readBytes : (n : Nat) → m {x : ByteArray // x.size ≤ n}

  /--
   - Read some block of bytes from the file.
   - An empty array is returned iff EOF has been reached.
   -/
  readBlock : m ByteArray := readBytes defaultBlockSize

  /-- Read a line of text as a string, returning `none` ifp EOF is reached. -/
  getLine : m (Option String)

  /-- Skip `n` bytes in the file. Will return the number of bytes skipped, being 0 iff EOF is reached. -/
  skip (skip : Nat) : m {skipped : Nat // skipped ≤ skip} := do
    let read ← readBytes skip
    pure ⟨read.val.size, by simp [read.property]⟩
export MonadFileRead (readBytes readBlock getLine skip)

instance liftMonadFileRead [MonadFileRead m] [Monad n] [MonadLift m n] : MonadFileRead n where
  readBytes n := readBytes (m := m) n |> monadLift
  readBlock := readBlock (m := m) |> monadLift
  skip x := skip (m := m) x |> liftM
  getLine := getLine (m := m) |> liftM

/-- Type of position in an input to seek from -/
inductive SeekWhence
  | fromStart
  | fromCurrent
  | fromEnd

/-- Class for monads with capabilities to read and seek to an arbitrary position of a file. -/
class MonadFileSeek (m : Type → Type _) extends MonadFileRead m where
  /-- Get the current byte offset from the start of the input. -/
  currentOffset : m Nat

  /-- 
   - Seek in the input. Failing if the resulting offset is passed any end of the input 
   - Returns the new absolute offset in the input.
   -/
  seek : (offset : Int) → SeekWhence → m Nat
export MonadFileSeek (currentOffset seek)

instance liftMonadFileSeek [MonadFileSeek m] [Monad n] [MonadLift m n] : MonadFileSeek n where
  currentOffset := currentOffset (m := m) |> monadLift
  seek x y := seek (m := m) x y |> liftM

/-- Class for monads with capabilities to read and write a file. -/
class MonadFileWrite (m : Type → Type _) extends MonadFileRead m where
  /-- Write an array of bytes. -/
  writeBytes : ByteArray → m Unit

  /-- Write some utf-8 encoded text to the file. -/
  putStr : String → m Unit

  /-- Flush the written content. -/
  flush : m Unit
export MonadFileWrite (writeBytes putStr flush)

instance liftMonadFileWrite [MonadFileWrite m] [Monad n] [MonadLift m n] : MonadFileWrite n where
  writeBytes x := writeBytes (m := m) x |> liftM
  putStr x := putStr (m := m) x |> liftM
  flush := flush (m := m) |> liftM

/-- Class for monads for read only file system operations. -/
class MonadReadFS (m : Type → Type u)
                  extends Monad m where
  /-- List all items in the current working directory. -/
  ls : (dir : FilePath := ".") → m (Array FilePath)

  /-- Get an absolute path to the current working directory. -/
  pwd : m FilePath

  /-- Change directory. -/
  cd : FilePath → m Unit

  /-- Get metadata about an item in the file system. -/
  metadata : FilePath → m IO.FS.Metadata

  /-- Check whether a path exists. -/
  pathExists : FilePath → m Bool

  /-- Check whether a path is a directory, returns false if it doesn't exists. -/
  isDir (path : FilePath) : m Bool := do
    if (← pathExists path)
    then (← metadata path).type == .dir |> pure
    else pure false

  /-- Type of monad for reading files. -/
  mfr : Type → Type u
  /-- `mfr` should implement `MonadFileRead` -/
  [readInst : MonadFileRead mfr]

  /-- State type when reading files. (Usually a file handle.) -/
  σ_r : Type
  /-- Error type when reading files. -/
  ε_r : Type

  /-- Open a file for reading. -/
  readFile : FilePath → Runner mfr m σ_r ε_r
export MonadReadFS (ls pwd cd metadata pathExists isDir mfr readFile)
attribute [instance] MonadReadFS.readInst

instance liftMonadReadFS [MonadReadFS m] [Monad n] [MonadLift m n] : MonadReadFS n where
  ls x := ls (m := m) x |> liftM
  pwd := pwd (m := m) |> liftM
  cd x := cd (m := m) x |> liftM
  metadata x := metadata (m := m) x |> liftM
  pathExists x := pathExists (m := m) x |> liftM
  isDir x := isDir (m := m) x |> liftM
  mfr := mfr (m := m)
  readFile path := Runner.lift (inst := readFile path)

/-- Class for monads for read and write file system operations. -/
class MonadWriteFS (m : Type → Type u)
                   extends MonadReadFS m where
  /-- Remove a file or empty directory. -/
  rm : FilePath → m Unit

  /-- Create a directory. -/
  createDir : FilePath → m Unit

  /-- Create a directory if it doesn't exists and create all missing parents. -/
  mkdir (path : FilePath) : m Unit := do
    for parent in path.components.inits.map mkFilePath do
      unless ← isDir parent do
        createDir parent
    pure ()
    
  /-- Type of monad for writing files. -/
  mfw : Type → Type u
  [writeInst : MonadFileWrite mfw]

  /-- Error type when writing files. -/
  ε_w : Type
  /-- State type when writing files. -/
  σ_w : Type

  /-- Open a file for writing. -/
  writeFile : FilePath → Runner mfw m σ_w ε_w
export MonadWriteFS (rm createDir mkdir mfw writeFile)
attribute [instance] MonadWriteFS.writeInst

instance liftMonadWriteFS [MonadWriteFS m] [Monad n] [MonadLift m n] : MonadWriteFS n where
  rm x := rm (m := m) x |> liftM
  createDir x := createDir (m := m) x |> liftM
  mkdir x := mkdir (m := m) x |> liftM
  mfw := mfw (m := m)
  writeFile path := Runner.lift (inst := writeFile (m := m) path)
end FS
