class Show (α : Type _) : Type _ where
  showMe : α → String
export Show (showMe)

@[default_instance low]
instance [ToString α] : Show α where
  showMe := toString

def showList [Show α] [ForIn (StateM String) ρ α] (xs : ρ) : String :=
  Prod.snd <| StateT.run (m := Id) (s := "") do
    for x in xs do
      modify fun s => s!"{s}{showMe x}\n"

instance [Show α] : Show (List α) := ⟨showList⟩
instance [Show α] : Show (Array α) := ⟨showList⟩

instance : Show System.FilePath where
  showMe x := x.toString

instance : Show IO.FS.DirEntry where
  showMe x :=
    if showMe x.root = "." then
      x.fileName
    else
      showMe <| System.FilePath.join x.root ⟨x.fileName⟩
