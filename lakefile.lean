import Lake
open Lake DSL

package leanshell where
  precompileModules := true

require mathlib from git
  "https://github.com/leanprover-community/mathlib4"

lean_lib Leanshell

@[default_target]
lean_exe leanshell where
  root := `Main

lean_exe test where
  root := `Test
