import Libarchive

def main : IO Unit := do
  IO.println <| Libarchive.version ()
  IO.println <| Libarchive.versionDetails ()
  IO.println <| Libarchive.app (fun x => x*2)
  IO.println <| Libarchive.pro (64, "Hurra!")
