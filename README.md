# Leanshell

## A strongly typed shell in Lean with full control over side effects.

This is a project to create a shell in the [Lean 4 programming language][1].
Where each command has clearly specified types for inputs and outputs and custom monad-classes for all kinds of side effects.

## Building

### Prerequisites

- The Lean 4 build tool [Lake][3], preferably as part of a recent Lean 4 toolchain installed with [Elan][2].
- The [Clang][4] C++ compiler. At least version 15.0.
- A recent version of [libc++][5], (an implementation of the C++ standard library).

Then just run:

```Bash
$ lake build
```

There is no REPL to use yet, so the best you can do is to edit the main function in `Main.lean` or install a Lean IDE in your editor and run commands with `#eval <COMMAND>` in any source file.

## File structure

- `Main.lean` contains the main function. (Nothing useful yet.)
- `Leanshell.lean` just imports all modules. (Not really interesting either.)
- `Leanshell/Control/*.lean` contains monad classes for different kinds of side effects.
- `lakefile.lean` is the Lean-kind-of a make file. See more at [Lake's homepage][3].


[1]: https://leanprover.github.io/about/
[2]: https://github.com/leanprover/elan
[3]: https://github.com/leanprover/lake
[4]: https://clang.llvm.org
[5]: https://libcxx.llvm.org
