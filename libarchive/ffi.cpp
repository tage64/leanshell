#include <archive.h>
#include <archive_entry.h>
#include <atomic>
#include <cassert>
#include <concepts>
#include <iostream>
#include <lean/lean.h>
#include <optional>
#include <sstream>
#include <string>

/// A flag telling whether some necessary things have been initialized in the Lean runtime.
static std::atomic_bool initialized_runtime = false;
/// A lock set to `true` when the runtime is in the process of being initialized to prevent 2
/// threads from trying to initialize simultainiously.
static std::atomic_bool began_initializing_runtime = false;
/// A pointer for a `lean_external_class` for an archive reader.
static lean_external_class *archive_read_class = nullptr;

/// A struct with callbacks and user data when reading an archive.
struct ArchiveReadData {
  struct archive *archive;
  lean_object *read_callback;
  lean_object *skip_callback;
  lean_object *close_callback;
  std::optional<lean_object *> seek_callback;
  lean_object *state;
  /// If any callback failes, the IO error will be stored in this pointer.
  /// That is, this lean object will be an IO computation that will fail.
  std::optional<lean_object *> maybe_error;
  /// Lean ByteArray holding the last read data.
  std::optional<lean_object *> read_buf;

  ArchiveReadData(
      lean_obj_arg read_callback, lean_obj_arg skip_callback, lean_obj_arg close_callback,
      std::optional<lean_obj_arg> seek_callback, lean_obj_arg initial_state
  )
      : archive(archive_read_new()), read_callback(read_callback), skip_callback(skip_callback),
        close_callback(close_callback), seek_callback(seek_callback), state(initial_state),
        maybe_error(std::nullopt), read_buf(std::nullopt) {}

  ~ArchiveReadData() {
    archive_read_free(this->archive);
    lean_dec(this->read_callback);
    lean_dec(this->skip_callback);
    if (this->seek_callback)
      lean_dec(*this->seek_callback);
    lean_dec(this->close_callback);
    lean_dec(this->state);
    if (this->maybe_error)
      lean_dec(*this->maybe_error);
    if (this->read_buf)
      lean_dec(*this->read_buf);
  }

  /// Take a return code from a libarchive function and check whether it signifies an error.
  /// Returns an optional with a lean object for an IO error if so is the case or an empty optional
  /// otherwise.
  std::optional<lean_obj_res> get_io_error(int ret_code) {
    std::ostringstream msg = std::ostringstream();
    switch (ret_code) {
    case ARCHIVE_OK:
    case ARCHIVE_WARN:
      return std::nullopt;
    case ARCHIVE_EOF:
      return lean_mk_io_error_eof(lean_box(0));
    case ARCHIVE_FATAL:
      if (this->maybe_error.has_value()) {
        lean_object *error = this->maybe_error.value();
        this->maybe_error.reset();
        return error;
      }
      msg << "Libarchive: Fatal: " << archive_error_string(this->archive);
      break;
    default:
      msg << "Libarchive: Unknown error code " << ret_code << ": (Errno "
          << archive_errno(this->archive) << ") " << archive_error_string(this->archive);
    }
    std::string msg_str = msg.str();
    return lean_mk_io_user_error(lean_mk_string_from_bytes(msg_str.data(), msg_str.size()));
  }
};

/// Check whether the Lean runtime is initialized with necessary things for
/// this module and initialize it if so is not the case.
void init_runtime() {
  if (!initialized_runtime.load()) {
    // The runtime was not initialized.
    if (began_initializing_runtime.exchange(true)) {
      // Someone else is in the process of initializing the runtime.
      while (!initialized_runtime.load()) {
      }
      return;
    }
    // Initialize the runtime.
    // We shall register a `lean_external_class` for an archive reader.
    lean_register_external_class(
        [](void *data_ptr) {
          ArchiveReadData *data = static_cast<ArchiveReadData *>(data_ptr);
          delete data;
        },
        [](auto, auto) {}
    );
    initialized_runtime.store(true);
  }
}

/// Get a reference to the `read_archive_class`, initialize it if necessary.
lean_external_class *get_archive_read_class() {
  init_runtime();
  return archive_read_class;
}

extern "C" lean_obj_res lean_archive_read_open(
    lean_obj_arg read_callback, lean_obj_arg skip_callback, lean_obj_arg maybe_seek_callback,
    lean_obj_arg close_callback, lean_obj_arg user_data, lean_obj_arg _io_world
) {
  // Define callbacks which will be passed to libarchive's open functions.
  // These are essentially wrappers around the Lean callbacks.

  /// This is a function which takes:
  /// @param run_state_io_comp A Lean function on the form `\sigma -> IO (a \times \sigma)`
  /// @param f A function taking a Lean object for `a` and returning some integral value `x`
  /// @param data A pointer to the `ArchiveReadData`
  /// @returns `x` or ARCHIVE_FATAL if the IO computation failed
  /// If the IO computation failed, `data->maybe_error` will be set to the error value.
  static auto lean_callback_wrapper =
      []<typename T>(lean_obj_arg state_run_io_comp, auto f, ArchiveReadData *data) -> T {
    assert(!data->maybe_error.has_value());
    // Discard the current content in data->read_buf:
    if (data->read_buf.has_value()) {
      lean_dec(data->read_buf.value());
      data->read_buf.reset();
    }

    // io_res : IO (a \times \sigma)
    lean_object *io_res = lean_apply_2(state_run_io_comp, data->state, lean_io_mk_world());
    if (lean_io_result_is_error(io_res)) {
      // The callback failed.
      data->maybe_error = io_res;
      return ARCHIVE_FATAL;
    }
    // res : a \times \sigma
    lean_object *res = lean_io_result_get_value(io_res);
    // data->state : \sigma
    data->state = lean_ctor_get(res, 1);
    lean_inc(data->state);
    // callback_res : a
    lean_object *callback_return_val = lean_ctor_get(res, 0);
    // Pass the return value of `lean_callback` to `f`.
    T x = f(callback_return_val);
    lean_dec(io_res);
    return x;
  };

  auto la_open_callback = [](auto, auto) { return ARCHIVE_OK; };
  auto la_read_callback = [](struct archive *, void *data_ptr, const void **buf) {
    ArchiveReadData *data = static_cast<ArchiveReadData *>(data_ptr);
    lean_inc(data->read_callback);
    return lean_callback_wrapper.operator()<ssize_t>(
        data->read_callback,
        [data, buf](b_lean_obj_arg read_bytes) {
          lean_inc(read_bytes);
          assert(!data->read_buf.has_value());
          data->read_buf = read_bytes;
          *buf = lean_sarray_cptr(read_bytes);
          return lean_sarray_size(read_bytes);
        },
        data
    );
  };
  auto la_skip_callback = [](struct archive *, void *data_ptr, int64_t request) {
    ArchiveReadData *data = static_cast<ArchiveReadData *>(data_ptr);
    lean_inc(data->skip_callback);
    return lean_callback_wrapper.operator()<int64_t>(
        lean_apply_1(data->skip_callback, lean_box_uint64(request)),
        [](lean_obj_arg skipped_bytes) { return lean_unbox(skipped_bytes); },
        data
    );
  };
  auto la_close_callback = [](struct archive *, void *data_ptr) {
    ArchiveReadData *data = static_cast<ArchiveReadData *>(data_ptr);
    lean_inc(data->close_callback);
    return lean_callback_wrapper.operator()<int>(
        data->close_callback, [](lean_obj_arg) -> int { return ARCHIVE_OK; }, data
    );
  };

  std::optional<lean_object *> maybe_lean_seek_callback;
  // Possibly a seek callback passed to libarchive.
  std::optional<archive_seek_callback *> maybe_la_seek_callback;
  switch (lean_ptr_tag(maybe_seek_callback)) {
  case 0:
    // No seek callback was provided.
    maybe_lean_seek_callback = std::nullopt;
    maybe_la_seek_callback = std::nullopt;
    break;
  case 1:
    maybe_lean_seek_callback = lean_ctor_get(maybe_seek_callback, 0);
    lean_inc(*maybe_lean_seek_callback);
    maybe_la_seek_callback = [](struct archive *, void *data_ptr, int64_t offset, int whence
                             ) -> int64_t {
      ArchiveReadData *data = static_cast<ArchiveReadData *>(data_ptr);
      uint8_t whence_idx;
      switch (whence) {
      case SEEK_SET:
        whence_idx = 0;
        break;
      case SEEK_CUR:
        whence_idx = 1;
        break;
      case SEEK_END:
        whence_idx = 2;
        break;
      default:
        assert(false);
      }
      lean_inc(data->seek_callback.value());
      return lean_callback_wrapper.operator()<int64_t>(
          lean_apply_2(
              data->seek_callback.value(), lean_big_int64_to_int(offset), lean_box(whence_idx)
          ),
          [](lean_obj_arg offset) { return lean_ctor_get_uint64(offset, 0); },
          data
      );
    };
  }
  lean_dec(maybe_seek_callback);

  ArchiveReadData *data = new ArchiveReadData(
      read_callback, skip_callback, close_callback, maybe_lean_seek_callback, user_data
  );

  if (auto io_error = data->get_io_error(archive_read_support_filter_all(data->archive))) {
    delete data;
    return *io_error;
  }
  if (auto io_error = data->get_io_error(archive_read_support_format_all(data->archive))) {
    delete data;
    return *io_error;
  }
  if (maybe_la_seek_callback) {
    if (auto io_error = data->get_io_error(
            archive_read_set_seek_callback(data->archive, *maybe_la_seek_callback)
        )) {
      delete data;
      return *io_error;
    }
  }
  if (auto io_error = data->get_io_error(archive_read_open2(
          data->archive,
          data,
          la_open_callback,
          la_read_callback,
          la_skip_callback,
          la_close_callback
      ))) {
    delete data;
    return *io_error;
  }

  return lean_io_result_mk_ok(lean_alloc_external(get_archive_read_class(), data));
}

/// lean_archive_read_next : ArchiveReader -> IO FilePath
extern "C" lean_obj_res lean_archive_read_next(b_lean_obj_arg data_obj, lean_obj_arg _io_world) {
  ArchiveReadData *data = static_cast<ArchiveReadData *>(lean_get_external_data(data_obj));
  struct archive_entry *entry;
  if (auto io_error = data->get_io_error(archive_read_next_header(data->archive, &entry))) {
    return *io_error;
  }
  const char *path = archive_entry_pathname_utf8(entry);
  if (path == NULL) {
    std::string msg = "Libarchive: No path or path not in UTF8";
    return lean_mk_io_user_error(lean_mk_string_from_bytes(msg.data(), msg.size()));
  }
  lean_object *lean_path = lean_mk_string(path);
  return lean_io_result_mk_ok(lean_path);
}

/// lean_archive_read : ArchiveReader -> IO ByteArray
extern "C" lean_obj_res lean_archive_read(b_lean_obj_arg data_obj, lean_obj_arg _io_world) {
  ArchiveReadData *data = static_cast<ArchiveReadData *>(lean_get_external_data(data_obj));
  const void *buf;
  size_t size;
  int64_t offset;
  if (auto io_error =
          data->get_io_error(archive_read_data_block(data->archive, &buf, &size, &offset))) {
    return *io_error;
  }
  lean_object *byte_array = lean_alloc_sarray(1, size, size);
  std::memcpy(lean_sarray_cptr(byte_array), buf, size);
  return lean_io_result_mk_ok(byte_array);
}
/// lean_archive_seek (offset : UInt64) (whence : SeekWhence) : ArchiveReader -> IO UInt64
///
/// Note that the type of the offset is UInt64, however, this is only because there isn't an easy
/// way to convert from Lean's Int type to int64_t. So we treat the offset as a 64-bit signed
/// integer with two's complement.
extern "C" lean_obj_res lean_archive_read_seek(
    const uint64_t offset_val, uint8_t lean_whence, b_lean_obj_arg data_obj, lean_obj_arg _io_world
) {
  ArchiveReadData *data = static_cast<ArchiveReadData *>(lean_get_external_data(data_obj));
  // Interpret the offset as a signed t64-bit two's complement integer.
  int64_t offset;
  std::memcpy(&offset, &offset_val, sizeof(offset_val));
  int whence;
  switch (lean_whence) {
  case 0:
    whence = SEEK_SET;
    break;
  case 1:
    whence = SEEK_CUR;
    break;
  case 2:
    whence = SEEK_END;
    break;
  default:
    assert(false);
  }
  int64_t res_offset = archive_seek_data(data->archive, offset, whence);
  if (auto io_error = data->get_io_error(res_offset)) {
    return *io_error;
  }
  return lean_io_result_mk_ok(lean_box((uint64_t)res_offset));
}

/// lean_archive_read_offset : ArchiveReader -> IO UInt64
/// 
/// TODO: Not really sure this is correct.
extern "C" lean_obj_res lean_archive_read_offset(b_lean_obj_arg data_obj, lean_obj_arg _io_world) {
  ArchiveReadData *data = static_cast<ArchiveReadData *>(lean_get_external_data(data_obj));
  int64_t offset = archive_read_header_position(data->archive);
  if (auto io_error = data->get_io_error(offset)) {
    return *io_error;
  }
  return lean_io_result_mk_ok(lean_box((uint64_t)offset));
}


extern "C" lean_obj_res lean_archive_version() { return lean_mk_string(archive_version_string()); }

extern "C" lean_obj_res lean_archive_version_details() {
  return lean_mk_string(archive_version_details());
}

extern "C" lean_obj_res apply_test(lean_obj_arg inst_1, lean_obj_arg f) {
  uint8_t x = 32;
  return lean_apply_1(f, lean_box(x));
}

extern "C" size_t prod_test(lean_obj_arg product) {
  lean_object *fst = lean_ctor_get(product, 0);
  size_t ret_val = lean_ctor_get_usize(fst, 0);
  lean_dec(product);
  return ret_val + 42;
}
