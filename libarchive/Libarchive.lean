import Leanshell.Control.FS
namespace Libarchive

@[extern "lean_archive_version"]
opaque version (a : Unit) : String

@[extern "lean_archive_version_details"]
opaque versionDetails (a : Unit) : String

private opaque ArchiveReaderPointed : NonemptyType
def ArchiveReader : Type := ArchiveReaderPointed.type
instance : Nonempty ArchiveReader := ArchiveReaderPointed.property

private def natToUInt64 (n : Nat) : IO UInt64 := do
  if h : n < UInt64.size then
    pure <| UInt64.ofNatCore n h
  else
    throw (IO.userError s!"Nat to big to convert to UInt64: {n}")

@[extern "lean_archive_read_open"]
private opaque rawArchiveReadOpen
    (readCallback : σ → IO (ByteArray × σ))
    (skipCallback : (skip : UInt64) → σ → IO ({ skipped : UInt64 // skipped ≤ skip} × σ))
    (seekCallback : Option ((offset : Int) → FS.SeekWhence → σ → IO (UInt64 × σ)))
    (closeCallback : σ → IO (Unit × σ))
    : σ → ArchiveReader

/--
 - Open an archive for reading.
 -/
def openRead [FS.MonadFileSeek (StateT σ IO)] (data : σ) : ArchiveReader :=
  rawArchiveReadOpen
    (StateT.run FS.readBlock)
    (fun skip => StateT.run do
      let ⟨skipped, skipped_le_skip⟩ ← FS.skip skip.toNat
      pure ⟨skipped.toUInt64, by sorry⟩ )
    (some <| fun offset whence => StateT.run do
      let newOffset ← FS.seek offset whence
      pure newOffset.toUInt64 )
    default
    data
  
/--
 - Open an archive for reading using only forward streaming, I.E. will never seek backwards.
 - Might not work for some formats like 7z.
 -/
def openReadStreaming [FS.MonadFileRead (StateT σ IO)] (data : σ) : ArchiveReader :=
  rawArchiveReadOpen
    (StateT.run FS.readBlock)
    (fun skip => StateT.run do
      let ⟨skipped, skipped_le_skip⟩ ← FS.skip skip.toNat
      pure ⟨skipped.toUInt64, by sorry⟩ )
    none
    default
    data
  

@[extern "lean_archive_read"]
opaque archive_read : ArchiveReader → IO ByteArray

-- What follows is just test functions which will be removed prior to release.
@[extern "apply_test"] opaque app [Inhabited α] (f : UInt8 → α) : α

@[extern "prod_test"] opaque pro : USize × σ → USize

class StreamM (m : Type _ → Type _) (σ : Type _) (α : outParam (Type _)) where
  next [Monad m] : σ → m (α × σ)

def StreamM.forIn [StreamM m σ α] [Monad m] (xs : σ) (b : β) (f : α → β → m (ForInStep β)) : m β := do
  let mut b := b
  let mut xs := xs
  repeat do
    let (x, xsNew) ← StreamM.next xs
    match ← f x b with
      | .yield bNew => do
        b := bNew
        xs := xsNew
      | .done bNew => do
        b := bNew
        break
  pure b
instance [StreamM m σ α] : ForIn m σ α where
  forIn := StreamM.forIn
end Libarchive
